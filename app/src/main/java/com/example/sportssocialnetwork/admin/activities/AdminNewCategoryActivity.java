package com.example.sportssocialnetwork.admin.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.categories.CategoryListActivity;
import com.example.sportssocialnetwork.main.MainActivity;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.example.sportssocialnetwork.utils.Util;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AdminNewCategoryActivity extends AppCompatActivity {

    @Bind(R.id.app_bar)Toolbar toolbar;
    @Bind(R.id.etCreateCategoryName)EditText etCreateCategoryName;
    @Bind(R.id.civCreateCategoryImage) CircleImageView civCreateCategoryImage;
    @Bind(R.id.tvAddImage)TextView tvAddImage;
    //@Bind(R.id.etCreateCategoryName)ProgressBar mProgress;

    private int mProgressStatus = 0;

    Bitmap bitmapCreateCategoryImage;
    private static int IMAGE_PICK_REQUEST = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_new_category);
        //Locate Toolbar
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        //setupview


         tvAddImage=(TextView)findViewById(R.id.tvAddImage);
        tvAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_REQUEST);
            }
        });
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == IMAGE_PICK_REQUEST)
            {
                String filePathMame = Util.getRealPathFromURI(imageReturnedIntent.getData(), this);
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmapCreateCategoryImage = (Bitmap) BitmapFactory.decodeFile(filePathMame, options);

                civCreateCategoryImage.setImageBitmap(bitmapCreateCategoryImage);
            }

        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_confirm:
                if (bitmapCreateCategoryImage == null) {
                    bitmapCreateCategoryImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher);
                }
                byte[] category_image = Util.BitmapToByteArray(bitmapCreateCategoryImage);
                final ParseFile photoImage=new ParseFile(category_image);
                photoImage.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            int category_count_member = 0;
                            String category_name = etCreateCategoryName.getText().toString().trim();
                            CategoryParse categoryParse = new CategoryParse(category_name, category_count_member, photoImage);
                            categoryParse.saveEventually(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Successful
                                        Intent intent = new Intent(AdminNewCategoryActivity.this, CategoryListActivity.class);
                                        ArrayList<CategoryParse> listOfCategories = new ArrayList<CategoryParse>();
                                        intent.putParcelableArrayListExtra("listOfCategories", listOfCategories);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    } else {
                                        //Failure
                                        Toast.makeText(AdminNewCategoryActivity.this,e.getMessage().toString(),Toast.LENGTH_LONG).show();

                                    }
                                }
                            });
                        } else {
                            Toast.makeText(AdminNewCategoryActivity.this,e.getMessage().toString(),Toast.LENGTH_LONG).show();
                        }
                    }
                });




                //  finish();
                break;
            case R.id.action_cancel:
                finish();
                break;
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                break;
        }


        return super.onOptionsItemSelected(item);
    }
}
