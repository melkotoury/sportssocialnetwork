package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/17/15.
 */
@ParseClassName("EventMaybeMembers")
public class EventMaybeMembersParse extends ParseObject implements Parcelable {




    public EventMaybeMembersParse(){
        super();
    }
    private ParseUser event_maybe_user_id;
    private EventParse event_id;


    public EventMaybeMembersParse(ParseUser event_maybe_user_id,EventParse event_id) {
        addMaybeMemberToEvent(event_maybe_user_id);
        setEventAssociated(event_id);
    }




    // Associate member with Event
    public void addMaybeMemberToEvent(ParseUser user) {
        put("event_maybe_user_id", user);
    }

    // Get the Maybe Comming user for this event
    public ParseUser getMaybeEventMember()  {
        return getParseUser("event_maybe_user_id");
    }



    // Associate each member with an Event
    public void setEventAssociated(EventParse eventParse) {
        put("event_id", eventParse);
    }

    // Get the Event for this Maybe comming Member
    public EventParse getEventforThisMaybeMember()  {
        return (EventParse) getParseObject("event_id");
    }



    public  final Parcelable.Creator<EventMaybeMembersParse> CREATOR
            = new Parcelable.Creator<EventMaybeMembersParse>() {
        public EventMaybeMembersParse createFromParcel(Parcel in) {
            return new EventMaybeMembersParse(in);
        }

        public EventMaybeMembersParse[] newArray(int size) {
            return new EventMaybeMembersParse[size];
        }
    };

    public EventMaybeMembersParse(Parcel in) {
        event_maybe_user_id =(ParseUser) in.readParcelable(ParseUser.class.getClassLoader());
        event_id = (EventParse)in.readParcelable(EventParse.class.getClassLoader());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) event_maybe_user_id, flags);
        dest.writeParcelable( event_id, flags);
    }

}
