package com.example.sportssocialnetwork.signup;

import android.util.Log;
import android.widget.Toast;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class SignupPresenterImpl implements ISignupPresenter, IonSignupFinishedListener {

    private ISignupView signupView;
    private ISignupInteractor signupInteractor;

    public SignupPresenterImpl(ISignupView signupView) {
        this.signupView = signupView;
        signupInteractor = new SignupInteractorImpl(this);
    }

    @Override
    public void validateCredentials(String fullName, String username, String password, String email) {
        signupInteractor.validateCredentials(fullName, username, password, email);
    }
    
    @Override
    public void OnIncompleteCredentials() {
        signupView.onIncompleteCredentials();
    }

    @Override
    public void onSignupSucess() {
        signupView.onSignupSucess();
        signupView.navigateToPersonalInfoActivity();
    }
    @Override
    public void onSignupFail(ParseException e) {
        if (e.getCode() == ParseException.USERNAME_TAKEN) {
            signupView.onUsernameTaken();
        } else if (e.getCode() == ParseException.INVALID_EMAIL_ADDRESS) {
            signupView.onInvalidEmail();
        } else if (e.getCode() == ParseException.EMAIL_TAKEN) {
            signupView.onEmailTaken();
        } else {
            signupView.onOtherError(e);
        }
    }
}
