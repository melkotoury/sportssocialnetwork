package com.example.sportssocialnetwork.friends;

import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 22.11.2015..
 */
public interface IFriendsPresenter {
    void fetchAllUsersFromParse( ArrayList<ParseObject> users,ArrayList<ParseObject> userRelations);
}
