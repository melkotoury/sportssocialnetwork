package com.example.sportssocialnetwork.components;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.sportssocialnetwork.friends.FriendsActivity;
import com.parse.ParsePushBroadcastReceiver;

/**
 * Created by Lenovo T420 on 24.9.2015..
 */
public class FriendsBroadcastReceiver extends ParsePushBroadcastReceiver {
    @Override
    protected void onPushOpen(Context context, Intent intent) {

        Log.d("FriendsBroadcastReceive","Push clicked");
        Intent i= new Intent(context, FriendsActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);

        //TODO  When user is logged out device still receives push message intended for last logged in user. Something must be wrong with logout
    }
}
