package com.example.sportssocialnetwork.friends;

import android.util.Log;

import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 22.11.2015..
 */
public class FriendsInteractorImpl implements IFriendsInteractor, FindCallback<ParseObject> {
    public static final String TAG = FriendsActivity.class.getSimpleName();

    private IonDownloadFriendsCompleteListener mListener;
    private  ArrayList<ParseObject> mUsers;
    private ArrayList<ParseObject> mUserRelations;

    public FriendsInteractorImpl(IonDownloadFriendsCompleteListener mListener) {
        this.mListener = mListener;
    }

    @Override
    public void fetchFriendsFromParse( ArrayList<ParseObject> users, ArrayList<ParseObject> userRelations) {
        mUsers = users;
        mUserRelations=userRelations;
            /*
         * Get ParseUsers using ParseUser.getQuery();
    	 */
        ParseQuery query = ParseUser.getQuery();
        query.orderByDescending("createdAt");
        query.findInBackground(this);

    }

    @Override
    public void done(List<ParseObject> users, ParseException e) {
        if (e == null) {
            users = removeCurrentUser(users);
            mUsers.clear();
            mUsers.addAll(users) ;
            Log.d(SportsSocialNetworkApplication.TAG, "no error in download users ");
            // Get user relations
            ParseRelation userRelations = ParseUser.getCurrentUser().getRelation("UserRelation");
            userRelations.getQuery().findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> userRelations, ParseException e) {
                    if (e == null) {
                        Log.d(SportsSocialNetworkApplication.TAG, "download friends sucess ");
                        mUserRelations.clear();
                        mUserRelations.addAll(userRelations);
                        mListener.onDownloadFriendsSucess();

                    } else if (e.getMessage().equals("java.lang.ClassCastException: java.lang.String cannot be cast to org.json.JSONObject")) {
                        //Do nothing since friends are not yet added.
                        //ignore
                        Log.d(SportsSocialNetworkApplication.TAG, "error= class cast:");
                        if (userRelations == null) {
                            mUserRelations.clear();

                            mListener.onUserHasNoFriends();
                            Log.d(SportsSocialNetworkApplication.TAG, "user hasno friends1:");

                        }
                    } else {
                        Log.e(TAG, "Exception caught!: " + e.getMessage(), e);
                        //  Log.d(TAG, "size of list! " + results.size());
                        if (userRelations == null) {
                            Log.d(SportsSocialNetworkApplication.TAG, "User has no friends2: " + e.getLocalizedMessage());

                            mListener.onUserHasNoFriends();
                        }
                    }
                }
            });
        } else {
            // Something went wrong.
            Log.d(SportsSocialNetworkApplication.TAG, "Error downloading users: " + e.getLocalizedMessage());
        }
    }

    private List<ParseObject> removeCurrentUser(List<ParseObject> objects) {
        ParseObject userToRemove = null;
        for (ParseObject user : objects) {
            if (user.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {
                userToRemove = user;
            }
        }
        if (userToRemove != null) {
            objects.remove(userToRemove);
        }
        return objects;
    }


}
