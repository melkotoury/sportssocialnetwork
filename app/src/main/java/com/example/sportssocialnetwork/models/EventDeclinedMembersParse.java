package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/17/15.
 */
@ParseClassName("EventDeclinedMembers")
public class EventDeclinedMembersParse extends ParseObject implements Parcelable {




    public EventDeclinedMembersParse(){
        super();
    }
    private ParseUser event_declined_user_id;
    private EventParse event_id;


    public EventDeclinedMembersParse(ParseUser event_declined_user_id,EventParse event_id) {
        addDeclinedMemberToEvent(event_declined_user_id);
        setEventAssociated(event_id);
    }




    // Associate member with Event
    public void addDeclinedMemberToEvent(ParseUser user) {
        put("event_declined_user_id", user);
    }

    // Get the Declined user for this event
    public ParseUser getDeclinedEventMember()  {
        return getParseUser("event_declined_user_id");
    }



    // Associate each member with an Event
    public void setEventAssociated(EventParse eventParse) {
        put("event_id", eventParse);
    }

    // Get the Event for this Declined Member
    public EventParse getEventforThisDeclinedMember()  {
        return (EventParse) getParseObject("event_id");
    }



    public  final Parcelable.Creator<EventDeclinedMembersParse> CREATOR
            = new Parcelable.Creator<EventDeclinedMembersParse>() {
        public EventDeclinedMembersParse createFromParcel(Parcel in) {
            return new EventDeclinedMembersParse(in);
        }

        public EventDeclinedMembersParse[] newArray(int size) {
            return new EventDeclinedMembersParse[size];
        }
    };

    public EventDeclinedMembersParse(Parcel in) {
        event_declined_user_id =(ParseUser) in.readParcelable(ParseUser.class.getClassLoader());
        event_id = (EventParse)in.readParcelable(EventParse.class.getClassLoader());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) event_declined_user_id, flags);
        dest.writeParcelable( event_id, flags);
    }

}
