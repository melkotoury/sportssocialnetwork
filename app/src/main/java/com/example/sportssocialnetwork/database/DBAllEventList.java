package com.example.sportssocialnetwork.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by melkotoury on 5/4/15.
 */
public class DBAllEventList {

    public DBAllEventList(Context context){

    }
    private static class AllEventListHelper extends SQLiteOpenHelper{

        /*
    private byte[] event_image; */
        public static final String TABLE_EVENT="Event" ;
        public static final String TABLE_PEOPLE="Event_People";
        public static final String COLUMN_OBJID="objectId";
        public static final String COLUMN_CREATED_AT="createdAt";
        public static final String COLUMN_UPDATED_AT="updatedAt";
        public static final String COLUMN_ACL="ACL";
        public static final String COLUMN_EVENT_TITLE="event_title";
        public static final String COLUMN_EVENT_DESCRIPTION="event_description";
        public static final String COLUMN_EVENT_LOCATION="event_location";
        public static final String COLUMN_EVENT_START_DATE="event_startDate";
        public static final String COLUMN_EVENT_START_TIME="event_startTime";
        public static final String COLUMN_EVENT_END_DATE="event_endDate";
        public static final String COLUMN_EVENT_END_TIME="event_endTime";
        public static final String COLUMN_IS_EVENT_DATE_PASSED="is_event_date_passed";
        public static final String COLUMN_EVENT_HOST_NAME="event_hostName";
        public static final String COLUMN_EVENT_HOST_ID="event_host_id";
        public static final String COLUMN_EVENT_HOST_ID_Pointer="username";

        //Create Table
        private static final String CREATE_TABLE_BOX_OFFICE = "CREATE TABLE " + TABLE_EVENT + " (" +
                COLUMN_OBJID + " TEXT," +
                COLUMN_CREATED_AT + " TEXT," +
                COLUMN_UPDATED_AT + " INTEGER," +
                COLUMN_ACL + " INTEGER," +
                COLUMN_EVENT_TITLE + " TEXT," +
                COLUMN_EVENT_DESCRIPTION + " TEXT," +
                COLUMN_EVENT_LOCATION + " TEXT," +
                COLUMN_EVENT_START_DATE + " TEXT," +
                COLUMN_EVENT_START_TIME + " TEXT," +
                COLUMN_EVENT_END_DATE + " TEXT" +
                COLUMN_EVENT_END_TIME + " TEXT," +
                COLUMN_IS_EVENT_DATE_PASSED + " TEXT," +
                COLUMN_EVENT_HOST_NAME + " TEXT" +
                COLUMN_EVENT_HOST_ID + " TEXT," +
                COLUMN_EVENT_HOST_ID_Pointer + " TEXT," +

                ");";

        private static final String DB_NAME = "event_db";
        private static final int DB_VERSION = 1;
        private Context mContext;


        public AllEventListHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
