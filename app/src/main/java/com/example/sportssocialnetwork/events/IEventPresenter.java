package com.example.sportssocialnetwork.events;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface IEventPresenter {
    void downloadAllEvents();
    void downloadHostingEvents();
}
