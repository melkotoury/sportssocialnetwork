package com.example.sportssocialnetwork.categories;

import android.util.Log;

import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class CategoryInteractorImpl implements  ICategoryInteractor,FindCallback<CategoryParse> {

    private static String TABLE_NAME = "Category";
    private ArrayList<CategoryParse> mListOfCategories;
    private IonDownloadCategoriesCompleteListener mListener;

    public CategoryInteractorImpl(IonDownloadCategoriesCompleteListener listener) {
        mListener=listener;
    }

    @Override
    public void obtainListOfCategories(ArrayList<CategoryParse> listOfCategories) {
        mListOfCategories=listOfCategories;

        ParseQuery<CategoryParse> query = ParseQuery.getQuery(TABLE_NAME);
        query.findInBackground(this);


    }

    @Override
    public void done(List<CategoryParse> list, ParseException e) {

        if(e==null) {
            mListOfCategories.clear();
            mListOfCategories.addAll(list);
            Log.d(SportsSocialNetworkApplication.TAG, "Broj elemenata u listi : " + list.size());
            mListener.onDownloadCategoriesSucess();
        }
        else
        {
            mListener.onDownloadCategoriesFailure(e);
        }
        }
}
