package com.example.sportssocialnetwork.callbacks;

import com.example.sportssocialnetwork.models.CategoryParse;

import java.util.ArrayList;

/**
 * Created by melkotoury on 5/14/15.
 */
public interface onRetreiveCategories {
    public ArrayList<CategoryParse> onRetrieveCategoryList();

}
