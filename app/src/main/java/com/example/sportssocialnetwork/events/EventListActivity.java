package com.example.sportssocialnetwork.events;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.astuetz.PagerSlidingTabStrip;
import com.example.sportssocialnetwork.activities.CreateEventActivity;
import com.example.sportssocialnetwork.activities.WelcomeActivity;
import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.callbacks.OnRetreivHostingeEvents;
import com.example.sportssocialnetwork.callbacks.OnRetreivInvitedEvents;
import com.example.sportssocialnetwork.callbacks.OnRetreiveEvents;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseUser;

import java.util.ArrayList;


public class EventListActivity extends BaseActivity
            implements  OnRetrieveEventFromParse,
                        OnRetreiveEvents,
                        OnRetreivHostingeEvents,
                        OnRetreivInvitedEvents{

   // private Toolbar toolbar;
    private ViewPager viewPager;
    private PagerSlidingTabStrip tabs;



    private static String TABLE_NAME = "Event";

    private ArrayList<EventParse> allEvents = new ArrayList<EventParse>();
    private ArrayList<EventParse> hostingEvents = new ArrayList<EventParse>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        setTitle("Event List");
        super.onCreateToolbar();
       // toolbar = (Toolbar) findViewById(R.id.app_bar);
      //  setSupportActionBar(toolbar);
       new RetrieveEventsListAsyncTask(allEvents, this).execute();
        new RetrieveHostingEventListAsyncTask(hostingEvents, EventListActivity.this).execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_create_event:
                startActivity(new Intent(this,CreateEventActivity.class));

                break;
            case R.id.action_refresh:

                //TODO refreshEventList for Invited and Host Events
            case R.id.action_settings:
                break;
            case R.id.action_disclaimer:
                FragmentManager disclaimerManager = getSupportFragmentManager();
                DisclaimerDialogFragment disclaimerDialogFragment = new DisclaimerDialogFragment();
                disclaimerDialogFragment.show(disclaimerManager, "MyDialog");
                break;
            case R.id.action_logout:
                //ParseUser.logOut();
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(EventListActivity.this, WelcomeActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();


                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public ArrayList<EventParse> onRetrieveAllEventsList() {

        return allEvents;
    }

    @Override
    public void OnCompletedTask() {
        //Start Fragment
       // AllEventListFragment allEventListFragment = AllEventListFragment.newInstance(this);

        viewPager=(ViewPager)findViewById(R.id.vp_event);
        viewPager.setAdapter(new EventViewPagerAdapter(getSupportFragmentManager()));
        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs_events);
        tabs.setBackgroundColor(getResources().getColor(R.color.primaryColor));
        tabs.setDividerColor(getResources().getColor(R.color.divider));
        tabs.setTextColor(getResources().getColor(R.color.icons));
        tabs.setIndicatorColor(getResources().getColor(R.color.accentColor));
        tabs.setDividerColor(getResources().getColor(R.color.accentColor));
        tabs.isTextAllCaps();



        tabs.setViewPager(viewPager);
        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public ArrayList<EventParse> onRetrieveHostingEventsList() {
        // Code for filtering Hosting events
        for (int i = 0; i < hostingEvents.size(); i++){
            if(hostingEvents.get(i).getEventHostName()!=ParseUser.getCurrentUser().getObjectId()){
                Log.d(SportsSocialNetworkApplication.TAG,"User id: "+ParseUser.getCurrentUser().getObjectId());
                hostingEvents.remove(i);



            }
        }
      //  new RetrieveHostingEventListAsyncTask(allEvents, this).execute();

        return hostingEvents;
    }

    @Override
    public ArrayList<EventParse> onRetrieveInvitedEventsList() {
        // Code for filtering invitedEvents
        return null;
    }
}
