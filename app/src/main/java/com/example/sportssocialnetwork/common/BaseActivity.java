package com.example.sportssocialnetwork.common;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.categories.CategoryListActivity;
import com.example.sportssocialnetwork.events.EventListActivity;
import com.example.sportssocialnetwork.friends.FriendsActivity;
import com.example.sportssocialnetwork.activities.WelcomeActivity;
import com.example.sportssocialnetwork.adapters.NavigationDrawerAdapter;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.newsFeed.NewsFeedActivity;
import com.parse.ParseUser;
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration;

public abstract class BaseActivity extends LifecycleLoggingActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private String mActivityTitle;

    private RecyclerView.Adapter drawerAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView drawerRecycleView;

    private ParseUser currentUser;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(SportsSocialNetworkApplication.TAG, "oncreate base class");
        //  setContentView(R.layout.navigation_drawer_layout);

    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.navigation_drawer_layout);
        Log.d(SportsSocialNetworkApplication.TAG, "inside setcontentview base class");
        ViewGroup content = (ViewGroup) findViewById(R.id.container);
        getLayoutInflater().inflate(layoutResID, content, true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        // onCreateToolbar();
        onCreateDrawer();
        mDrawerToggle.syncState();
    }

    protected void onCreateToolbar() {
        //Locate Toolbar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

    }

    protected void onCreateDrawer() {
        drawerRecycleView = (RecyclerView) findViewById(R.id.drawer_recycler_view);  //ContextCompat.getDrawable(this, R.drawable.your_drawable)
        drawerRecycleView.setHasFixedSize(true);                    //getResources().getDrawable(R.drawable.abc_list_divider_mtrl_alpha))
        drawerRecycleView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this).color(R.color.divider).build());

        final String TITLES[] = {"Home", "Friends", "Events", "Categories", "Todo"};
        final int ICONS[] = {R.drawable.ic_action_home, R.drawable.ic_action_friends, R.drawable.ic_action_events, R.drawable.ic_action_groups, R.drawable.ic_travel};
        currentUser = ParseUser.getCurrentUser();
        final String NAME = currentUser.getString("fullname");
        final String EMAIL = currentUser.getEmail();



        int PROFILE = R.drawable.ic_launcher;
        drawerAdapter = new NavigationDrawerAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE);
        drawerRecycleView.setAdapter(drawerAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        drawerRecycleView.setLayoutManager(mLayoutManager);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mActivityTitle = getTitle().toString();
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {

            public void onDrawerOpened(View drawerView) {

                getSupportActionBar().setTitle("Navigation");
                // super.onDrawerOpened(drawerView);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerClosed(View drawerView) {
                getSupportActionBar().setTitle(mActivityTitle);
                //super.onDrawerClosed(drawerView);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

        };

        final GestureDetector mGestureDetector = new GestureDetector(getBaseContext(), new GestureDetector.SimpleOnGestureListener() {

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        }
        );
        drawerRecycleView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                View child = rv.findChildViewUnder(e.getX(), e.getY());
                if (child != null && mGestureDetector.onTouchEvent(e)) {
                    drawerLayout.closeDrawers();
                    int id = drawerRecycleView.getChildAdapterPosition(child);
                    Log.d(SportsSocialNetworkApplication.TAG, "The Item Clicked is: " + id);


                    switch (id) {
                        case 1:
                             openActivity(getBaseContext(), NewsFeedActivity.class);
                            break;
                        case 2:
                            openActivity(getBaseContext(), FriendsActivity.class);

                            break;
                        case 3:
                            openActivity(getBaseContext(), EventListActivity.class);

                            break;
                        case 4:
                           openActivity(getBaseContext(),CategoryListActivity.class);
                            // openActivity(GroupsActivity.class);

                            break;

                    }
                    return true;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {

            }
        });
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        //   mDrawerToggle.syncState();
    }





    private void openActivity(Context context, Class activity) {
        Intent intent = new Intent(
                context,
                activity);
        startActivity(intent);
        finish();


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        switch (id) {
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(BaseActivity.this, WelcomeActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}
