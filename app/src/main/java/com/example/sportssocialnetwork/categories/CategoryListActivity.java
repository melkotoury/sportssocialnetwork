package com.example.sportssocialnetwork.categories;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.activities.WelcomeActivity;
import com.example.sportssocialnetwork.admin.activities.AdminNewCategoryActivity;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.fragments.SuggestCategoryDialogFragment;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CategoryListActivity extends BaseActivity implements ICategoryView {


    @Bind(R.id.srCategoryList) SwipeRefreshLayout srCategoryList;
    @Bind(R.id.rvCategoryList) RecyclerView rvCategoryList;

    private CategoryListAdapter categoryListAdapter;
    private LayoutManager mLayoutManager;
    private ArrayList<CategoryParse> listOfCategories=new ArrayList<CategoryParse>();
    private ICategoryPresenter mPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(SportsSocialNetworkApplication.TAG, "oncreate child class");
        setContentView(R.layout.activity_category_list);
        super.onCreateToolbar();

        mPresenter= new CategoryPresenterImpl(this);
        mPresenter.downloadListOfCategories(listOfCategories);

        setUpView();
    }

    public void setUpView() {
        setTitle("Category List");
        ButterKnife.bind(this);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvCategoryList.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rvCategoryList.setLayoutManager(mLayoutManager);
        categoryListAdapter = new CategoryListAdapter(listOfCategories, this);
        rvCategoryList.setAdapter(categoryListAdapter);

    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();


        srCategoryList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                mPresenter.downloadListOfCategories(listOfCategories);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_create_event:
                ParseUser adminUser=ParseUser.getCurrentUser();
                if (adminUser.getUsername().equals("melkotoury")){
                    Toast.makeText(this,"Admin",Toast.LENGTH_LONG).show();

                    startActivity(new Intent(this,AdminNewCategoryActivity.class));
                }
                else {
                    Toast.makeText(this,adminUser.getUsername(),Toast.LENGTH_LONG).show();
                   // Toast.makeText(this,"Not Admin",Toast.LENGTH_LONG).show();
                    FragmentManager suggestCategoryToAdminManager = getSupportFragmentManager();
                    SuggestCategoryDialogFragment suggestCategoryDialogFragment = new SuggestCategoryDialogFragment();
                    suggestCategoryDialogFragment.show(suggestCategoryToAdminManager, "Please Suggest New Category");
                }

                break;
            case R.id.action_refresh:

                //TODO refreshEventList for Invited and Host Events
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager disclaimerManager = getSupportFragmentManager();
                DisclaimerDialogFragment disclaimerDialogFragment = new DisclaimerDialogFragment();
                disclaimerDialogFragment.show(disclaimerManager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(CategoryListActivity.this, WelcomeActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDownloadCategoriesSucess() {
        if(srCategoryList.isRefreshing()) srCategoryList.setRefreshing(false);
        categoryListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDownloadCategoriesError(ParseException e) {
        if(srCategoryList.isRefreshing()) srCategoryList.setRefreshing(false);
        Toast.makeText(this,e.getLocalizedMessage(),Toast.LENGTH_LONG).show();
        Log.d(SportsSocialNetworkApplication.TAG,e.getLocalizedMessage());
    }
}