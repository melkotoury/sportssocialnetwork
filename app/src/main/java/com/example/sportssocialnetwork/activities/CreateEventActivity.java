package com.example.sportssocialnetwork.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.andreabaccega.widget.FormEditText;
import com.example.sportssocialnetwork.R;
//import com.example.sportssocialnetwork.utils.Util;
import com.example.sportssocialnetwork.events.EventListActivity;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.example.sportssocialnetwork.utils.Util;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CreateEventActivity extends AppCompatActivity {
    private Toolbar toolbar;

  private static int IMAGE_PICK_REQUEST = 101;
    private String username, creatorId;
   private Bitmap bitmapCreateEventImage;

    @Bind(R.id.spCreateEventCategory) Spinner spCreateEventCategory;
    @Bind(R.id.tvAddImage) TextView tvAddImage;
    @Bind(R.id.ivCreateEventImage) ImageView ivCreateEventImage;

    @Bind(R.id.etCreateEventName) FormEditText etCreateEventName;
    @Bind(R.id.etCreateEventDetails) FormEditText etCreateEventDetails;
    @Bind(R.id.etCreateEventLocation) FormEditText etCreateEventLocation;
    @Bind(R.id.etCreateEventDateStart) FormEditText etCreateEventDateStart;
    @Bind(R.id.etCreateEventTimeStart) FormEditText etCreateEventTimeStart;
    @Bind(R.id.etCreateEventDateEnd) FormEditText etCreateEventDateEnd;
    @Bind(R.id.etCreateEventTimeEnd) FormEditText etCreateEventTimeEnd;




    final Calendar c = Calendar.getInstance();
    final Calendar c2=Calendar.getInstance();



    private ArrayList<String> listOfCategories=new ArrayList<String>();


    private static String TABLE_NAME = "Category";
    private static String CATEGORY_NAME_COLUMN = "category_name";




    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        tvAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_REQUEST);
            }
        });

        etCreateEventDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                new DatePickerDialog(CreateEventActivity.this, startDate, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        etCreateEventTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(CreateEventActivity.this, startTime, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), false).show();
            }
        });

        etCreateEventDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(v);
                new DatePickerDialog(CreateEventActivity.this, EndDate, c2.get(Calendar.YEAR), c2.get(Calendar.MONTH), c2.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etCreateEventTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(CreateEventActivity.this, EndTime, c2.get(Calendar.HOUR), c2.get(Calendar.MINUTE), false).show();
            }
        });

        ParseQuery query = new ParseQuery(TABLE_NAME);
        List<String> localList = new ArrayList<String>();
        try {
            List<ParseObject> l = query.find();


            for (ParseObject categoryParseObject : l) {
                CategoryParse newCategory = new CategoryParse();
                String categoryName=categoryParseObject.getString(CATEGORY_NAME_COLUMN);
                localList.add(categoryName);
            }

            listOfCategories.clear();
            listOfCategories.addAll(localList);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>
                    (this, android.R.layout.simple_spinner_item,listOfCategories);

            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);



            // spCreateEventCategory.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            //spCreateEventCategory.setBackgroundColor(getResources().getColor(R.color.primaryColor));

            spCreateEventCategory.setAdapter(dataAdapter);
            spCreateEventCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (com.parse.ParseException e) {
        }
    }


    public void hideKeyboard(View view) {


        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }


    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setCurrentDateOnView();
        }
    };

    TimePickerDialog.OnTimeSetListener startTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            setCurrentDateOnView();
        }
    };




    DatePickerDialog.OnDateSetListener EndDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c2.set(Calendar.YEAR, year);
            c2.set(Calendar.MONTH, monthOfYear);
            c2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setCurrentDateOnView();
        }
    };

    TimePickerDialog.OnTimeSetListener EndTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            c2.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c2.set(Calendar.MINUTE, minute);
            setCurrentDateOnView();
        }
    };


    public void setCurrentDateOnView() {
        String dateFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
        etCreateEventDateStart.setText(sdf.format(c.getTime()));
        etCreateEventDateEnd.setText(sdf.format(c2.getTime()));

        String timeFormat = "hh:mm: a";
        SimpleDateFormat stf = new SimpleDateFormat(timeFormat, Locale.US);
        etCreateEventTimeStart.setText(stf.format(c.getTime()));

        etCreateEventTimeEnd.setText(stf.format(c2.getTime()));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == IMAGE_PICK_REQUEST)
            {
                String filePathMame = Util.getRealPathFromURI(imageReturnedIntent.getData(), this);
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmapCreateEventImage = (Bitmap) BitmapFactory.decodeFile(filePathMame, options);

                ivCreateEventImage.setImageBitmap(bitmapCreateEventImage);
            }

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_create_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_confirm:

                FormEditText[] allFields    = { etCreateEventName, etCreateEventDetails, etCreateEventLocation,
                        etCreateEventDateStart, etCreateEventTimeStart, etCreateEventDateEnd, etCreateEventTimeEnd };


                boolean allValid = true;
                for (FormEditText field: allFields) {
                    allValid = field.testValidity() && allValid;
                }

                if (allValid) {
                    // YAY


                    if(bitmapCreateEventImage==null)
                    {
                        bitmapCreateEventImage=BitmapFactory.decodeResource(getResources(),R.drawable.ic_launcher);
                    }
                    byte[] event_image=Util.BitmapToByteArray(bitmapCreateEventImage);
                   final ParseFile photoFile=new ParseFile(event_image);
                    photoFile.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            String event_title = etCreateEventName.getText().toString().trim();
                            String event_category = spCreateEventCategory.getSelectedItem().toString().trim();
                            String event_description = etCreateEventDetails.getText().toString().trim();
                            String event_location = etCreateEventLocation.getText().toString().trim();
                            String event_startDate = etCreateEventDateStart.getText().toString().trim();
                            String event_startTime = etCreateEventTimeStart.getText().toString().trim();
                            String event_endDate = etCreateEventDateEnd.getText().toString().trim();
                            String event_endTime = etCreateEventTimeEnd.getText().toString().trim();
                            boolean is_event_date_passed = false;
                            String event_hostName = ParseUser.getCurrentUser().getUsername();
                            String event_host_id = ParseUser.getCurrentUser().getObjectId();
                            EventParse eventParse = new EventParse(event_title, event_category, event_description, event_location
                                    , event_startDate, event_startTime, event_endDate, event_endTime, is_event_date_passed,
                                    event_hostName, photoFile, event_host_id);
                            eventParse.setOwner(ParseUser.getCurrentUser());
                            eventParse.saveEventually(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //Event created successfully
                                        Toast.makeText(CreateEventActivity.this, "Created Successfuly", Toast.LENGTH_SHORT);
                                        startActivity(new Intent(CreateEventActivity.this, EventListActivity.class));
                                        finish();

                                    } else {
                                        //Something Went Wrong
                                    }
                                }
                            });
                        }
                    });

                } else {
                    // EditText are going to appear with an exclamation mark and an explicative message.
                }






              //  finish();
                break;
            case R.id.action_cancel:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_cancel_create_event_title)
                        .content(R.string.material_cancel_create_event_content)
                        .positiveText(R.string.material_cancel_create_event_agree)
                        .negativeText(R.string.material_cancel_create_event_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                startActivity(new Intent(CreateEventActivity.this, EventListActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();                break;
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(CreateEventActivity.this, WelcomeActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
