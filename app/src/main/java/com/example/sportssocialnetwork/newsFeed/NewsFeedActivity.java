package com.example.sportssocialnetwork.newsFeed;

import android.content.res.Configuration;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.util.Log;
import android.widget.Toast;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.adapters.AllEventListAdapter;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseException;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class NewsFeedActivity extends BaseActivity implements INewsFeedView,SwipeRefreshLayout.OnRefreshListener {

    private AllEventListAdapter newsFeedListAdapter;

    @Bind(R.id.rvNewsFeedList)
    RecyclerView rvNewsFeedList;
    @Bind(R.id.srNewsFeedList)
    SwipeRefreshLayout srNewsFeedList;

    private LayoutManager mLayoutManager;
    private ArrayList<EventParse> listOfEvents = new ArrayList<EventParse>();

    public static String TABLE_NAME = "Event";
    public static String EVENT_NAME_COLUMN = "event_title";
    public static String EVENT_IMAGE_COLUMN = "event_image";
    public static String EVENT_HOST_NAME_COLUMN = "event_hostName";
    public static String EVENT_START_DATE_COLUMN = "event_startDate";

    private INewsFeedPresenter mNewsFeedPresenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(SportsSocialNetworkApplication.TAG, "oncreate child class");
        setContentView(R.layout.activity_news_feed);
        super.onCreateToolbar();


        mNewsFeedPresenter= new NewsFeedPresenterImpl(this);
        setUpView();

    }
    public void setUpView() {
        setTitle("News Feed");
        ButterKnife.bind(this);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvNewsFeedList.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rvNewsFeedList.setLayoutManager(mLayoutManager);

        // specify an adapter
        newsFeedListAdapter = new AllEventListAdapter(listOfEvents, this);
        rvNewsFeedList.setAdapter(newsFeedListAdapter);

        mNewsFeedPresenter.downloadNewsFeedList(listOfEvents);
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();

        srNewsFeedList.setOnRefreshListener(this);
    }
    @Override
    public void onRefresh() {
        // Refresh items
        mNewsFeedPresenter.downloadNewsFeedList(listOfEvents);
    }
    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }
 // methods from INewsFeedView
    @Override
    public void onDownloadNewsComplete() {
        // specify an adapter
        newsFeedListAdapter.notifyDataSetChanged();
       if(srNewsFeedList.isRefreshing()) srNewsFeedList.setRefreshing(false);
    }

    @Override
    public void onDownloadNewsError(ParseException e) {
        if(srNewsFeedList.isRefreshing()) srNewsFeedList.setRefreshing(false);
        Toast.makeText(this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
        Log.d(SportsSocialNetworkApplication.TAG,e.getLocalizedMessage());
    }
}