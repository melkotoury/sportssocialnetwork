package com.example.sportssocialnetwork.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.events.EventListActivity;
import com.example.sportssocialnetwork.adapters.AllEventListAdapter;
import com.example.sportssocialnetwork.callbacks.HostEventListLoadListener;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HostingEventListFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private AllEventListAdapter hosEventListAdapter;
    private RecyclerView rvAllEventList;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button bConfirm,bMaybe,bDecline;
    private SwipeRefreshLayout srAllEventList;
    private HostEventListLoadListener mhostEventCallback;
    private List<ParseObject> eventsParseList;
    private ArrayList<EventParse> listOfEvents;

    private static String TABLE_NAME = "Event";
    private static String COLUMN_EVENT_TITLE = "event_title";
    private static String COLUMN_EVENT_HOST_ = "event_hostName";
    private static String COLUMN_EVENT_DATE = "event_startDate";
    private static String COLUMN_EVENT_IMAGE = "image";

    public HostingEventListFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentBoxOffice.
     */
    // TODO: Rename and change types and number of parameters
    public static HostingEventListFragment newInstance(String param1,String param2){
        HostingEventListFragment hostingEventListFragment=new HostingEventListFragment();
        Bundle args=new Bundle();
        //put any extra arguments that you may want to supply to this fragment
        hostingEventListFragment.setArguments(args);
        return hostingEventListFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_hosting_event_list, container, false);
        srAllEventList=(SwipeRefreshLayout)view.findViewById(R.id.srHostEventlist);
        rvAllEventList=(RecyclerView)view.findViewById(R.id.rvHostEventList);
        //set the layout manager before trying to display data
        rvAllEventList.setLayoutManager(new LinearLayoutManager(getActivity()));
        listOfEvents = ((EventListActivity)getActivity()).onRetrieveHostingEventsList();

        //Initialize adapter
        hosEventListAdapter=new AllEventListAdapter(listOfEvents, getActivity());
        rvAllEventList.setAdapter(hosEventListAdapter);





        //update your Adapter to containg the retrieved movies
        hosEventListAdapter.setlistOfEvents(listOfEvents);


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        srAllEventList.setOnRefreshListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);


    }


    @Override
    public void onRefresh() {
        //TODO onRefresh bug fix:Bad Behaviour when Refreshing  because of the for loop
        listOfEvents = ((EventListActivity)getActivity()).onRetrieveHostingEventsList();

        hosEventListAdapter.notifyDataSetChanged();
    }
}
