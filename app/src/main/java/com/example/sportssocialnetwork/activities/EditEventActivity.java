package com.example.sportssocialnetwork.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.main.MainActivity;
import com.example.sportssocialnetwork.utils.Util;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EditEventActivity extends AppCompatActivity {

    private String username, creatorId;

    private ImageView ivEditEventImage;
    private Bitmap bitmapEditEventImage;

    @Bind(R.id.app_bar)  Toolbar toolbar;
    @Bind(R.id.tvAddImage) TextView tvAddImage;
    @Bind(R.id.etEditEventName) EditText etEditEventName;
    @Bind(R.id.etEditEventDetails) EditText etEditEventDetails;
    @Bind(R.id.etEditEventLocation) EditText etEditEventLocation;
    @Bind(R.id.etEditEventDateStart) EditText etEditEventDateStart;
    @Bind(R.id.etEditEventTimeStart) EditText etEditEventTimeStart;
    @Bind(R.id.etEditEventDateEnd) EditText etEditEventDateEnd;
    @Bind(R.id.etEditEventTimeEnd) EditText etEditEventTimeEnd;

    private static int IMAGE_PICK_REQUEST = 101;
    final Calendar c = Calendar.getInstance();
    final Calendar c2=Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_event);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        tvAddImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_REQUEST);
            }
        });
        etEditEventDateStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditEventActivity.this, startDate, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        etEditEventTimeStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(EditEventActivity.this, startTime, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), false).show();
            }
        });
        etEditEventDateEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditEventActivity.this, EndDate, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        etEditEventTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(EditEventActivity.this, EndTime, c.get(Calendar.HOUR), c.get(Calendar.MINUTE), false).show();
            }
        });

    }

    DatePickerDialog.OnDateSetListener startDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setCurrentDateOnView();
        }
    };

    TimePickerDialog.OnTimeSetListener startTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            c.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c.set(Calendar.MINUTE, minute);
            setCurrentDateOnView();
        }
    };


    DatePickerDialog.OnDateSetListener EndDate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c2.set(Calendar.YEAR, year);
            c2.set(Calendar.MONTH, monthOfYear);
            c2.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setCurrentDateOnView();
        }
    };

    TimePickerDialog.OnTimeSetListener EndTime = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            c2.set(Calendar.HOUR_OF_DAY, hourOfDay);
            c2.set(Calendar.MINUTE, minute);
            setCurrentDateOnView();
        }
    };

    public void setCurrentDateOnView() {
        String dateFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
        etEditEventDateStart.setText(sdf.format(c.getTime()));
        etEditEventDateEnd.setText(sdf.format(c2.getTime()));

        String timeFormat = "hh:mm: a";
        SimpleDateFormat stf = new SimpleDateFormat(timeFormat, Locale.US);
        etEditEventTimeStart.setText(stf.format(c.getTime()));

        etEditEventTimeEnd.setText(stf.format(c2.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == IMAGE_PICK_REQUEST)
            {
                String filePathMame = Util.getRealPathFromURI(imageReturnedIntent.getData(), this);
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmapEditEventImage = (Bitmap) BitmapFactory.decodeFile(filePathMame, options);
                ivEditEventImage.setImageBitmap(bitmapEditEventImage);
            }
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_confirm:
                finish();
                break;
            case R.id.action_cancel:
                finish();
                break;
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }
}
