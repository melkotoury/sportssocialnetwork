package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;
import com.parse.ParseClassName;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/10/15.
 */
@ParseClassName("Category")
public class CategoryParse extends ParseObject implements Parcelable {
    private String category_name;
    private int category_count_members;
    private ParseFile  category_image;


    // Ensure that your subclass has a public default constructor
    public CategoryParse(){super();}

    // Add a constructor that contains core properties
    public CategoryParse(String category_name,int category_count_members,ParseFile category_image){
        super();
        setCategoryName(category_name);
        setCategoryCountMembers(category_count_members);
        setCategoryImage(category_image);
    }
    //setters to set values to the backend using put
    public void setCategoryName(String category_name)
    {
        put("category_name",category_name);
    }

    public void setCategoryImage(ParseFile category_image) {
        put("category_image",category_image);

    }

    public void setCategoryCountMembers(int category_count_members) {
        put("category_count_members",category_count_members);

    }

    //use getters to access the fields by getString,getBoolean,getArrayByte

    public String getCategoryName() {

        return getString("category_name");
    }

    public ParseFile getCategoryImage() {

        return getParseFile("category_image");
    }

    public int getCategoryCountMembers() {
        return getInt("category_count_members");
    }


    //Get the user for this item
    public ParseUser getUser()  {
        return getParseUser("owner");
    }

    //Assosciate each ite with a user
    public void setOwner(ParseUser user){
        put("owner",user);
    }



    public static final Parcelable.Creator<CategoryParse> CREATOR
            = new Parcelable.Creator<CategoryParse>() {
        public CategoryParse createFromParcel(Parcel in) {
            return new CategoryParse(in);
        }

        public CategoryParse[] newArray(int size) {
            return new CategoryParse[size];
        }
    };

    private CategoryParse(Parcel in) {
        category_name = in.readString();
        category_count_members = in.readInt();
        category_image = in.readParcelable(getClass().getClassLoader());

        // category_image = new byte[in.readInt()];
        //in.readByteArray(category_image);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category_name);
        dest.writeInt(category_count_members);
        //dest.writeInt(category_image.length);
       // dest.writeByteArray(category_image);
        dest.writeParcelable((Parcelable) category_image, flags);


    }
}
