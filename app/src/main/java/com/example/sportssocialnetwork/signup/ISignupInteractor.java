package com.example.sportssocialnetwork.signup;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface ISignupInteractor {
    void validateCredentials(String fullname,String username,String password,String email);
    void signUpWithParse();
}
