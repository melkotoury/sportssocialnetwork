package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/17/15.
 */
@ParseClassName("CategoryMembers")
public class CategoryMembersParse extends ParseObject implements Parcelable {

    public CategoryMembersParse(){
        super();
    }
    private String category_user_id;
    private String category_id;

    // Associate member with category
    public void AddMemberToCategory(ParseUser user) {
        put("category_user_id", user);
    }

    // Get the user for this Category
    public ParseUser getCategoryMember()  {
        return getParseUser("category_user_id");
    }



    // Associate each member with a category
    public void setCategoryAssociated(CategoryParse categoryParse) {
        put("category_id", categoryParse);
    }

    // Get the Category for this item
    public CategoryParse getCategoryforThisMember()  {
        return (CategoryParse) getParseObject("category_id");
    }

    public  final Parcelable.Creator<CategoryMembersParse> CREATOR
            = new Parcelable.Creator<CategoryMembersParse>() {
        public CategoryMembersParse createFromParcel(Parcel in) {
            return new CategoryMembersParse(in);
        }

        public CategoryMembersParse[] newArray(int size) {
            return new CategoryMembersParse[size];
        }
    };


    private CategoryMembersParse(Parcel in) {
        category_user_id = in.readString();
        category_id = in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(category_user_id);
        dest.writeString(category_id);

    }
}
