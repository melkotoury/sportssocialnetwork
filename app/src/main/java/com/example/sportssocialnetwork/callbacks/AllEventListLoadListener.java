package com.example.sportssocialnetwork.callbacks;

import com.example.sportssocialnetwork.models.EventParse;

import java.util.ArrayList;

/**
 * Created by melkotoury on 5/4/15.
 */
public interface AllEventListLoadListener {
    public void onAllEventListLoaded(EventParse eventParse);
}
