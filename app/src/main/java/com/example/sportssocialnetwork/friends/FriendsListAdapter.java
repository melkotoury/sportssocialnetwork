package com.example.sportssocialnetwork.friends;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sportssocialnetwork.R;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseRelation;
import com.parse.ParseUser;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 21.9.2015..
 */
public class FriendsListAdapter extends ArrayAdapter<ParseObject> {

    public static final String TAG = FriendsListAdapter.class.getSimpleName();

    protected Context mContext;
    protected LayoutInflater mInflater;
    protected ArrayList<ParseObject>  mUsers;
    protected ArrayList<ParseObject> mUserRelations;

    private final String RELATION = "UserRelation";

    public FriendsListAdapter(Context context,  ArrayList<ParseObject> users, ArrayList<ParseObject> userRelations) {
        super(context, R.layout.friends_list_item, users);
        mContext = context;
        mUsers = users;
        mUserRelations = userRelations;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.friends_list_item, null);

            holder = new ViewHolder();
            holder.usernameLabel = (TextView) convertView.findViewById(R.id.usernameLabel);
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.userImage);
            holder.checkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final ParseObject user = mUsers.get(position);


        holder.usernameLabel.setText(user.getString("username"));

        holder.checkbox.setChecked(false);
        // if(mUserRelations!=null) {
        for (int i = 0; i < mUserRelations.size(); i++) {
            if (mUserRelations.get(i).getObjectId().equals(user.getObjectId())) {
                holder.checkbox.setChecked(true);
            }
        }
        // }

        holder.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkbox = (CheckBox) v;
                if (checkbox.isChecked()) {
                    updateRelationship(user, true);
                } else {
                    updateRelationship(user, false);
                }
            }
        });

        return convertView;
    }

    protected void updateRelationship(ParseObject user, boolean shouldAdd) {
        /*
		 * Update relationships using ParseUser.getRelation()
		 */
        ParseUser currentUser = ParseUser.getCurrentUser();
        ParseRelation userRelation = currentUser.getRelation(RELATION);
        if (shouldAdd) {


          //  Log.d("FriendsListAdapter", userToFollow.getObjectId())    ;
            sendPushNotifications(user);

            userRelation.add(user);
            mUserRelations.add(user);

        } else {
            userRelation.remove(user);
            for (int i = 0; i < mUserRelations.size(); i++) {
                if (mUserRelations.get(i).getObjectId().equals(user.getObjectId())) {
                    mUserRelations.remove(i);
                }
            }
        }
        currentUser.saveInBackground();
    }

    protected void sendPushNotifications(ParseObject user) {
        ParseQuery<ParseInstallation> query = ParseInstallation.getQuery();

        ParseUser userToFollow = (ParseUser) user;
        //query.whereContainedIn("userId", Arrays.asList(user));
        query.whereContains("userId",userToFollow.getObjectId());
        // send push notification
        ParsePush push = new ParsePush();
        push.setQuery(query);
        push.setMessage("User "+ParseUser.getCurrentUser().getUsername()+" is now following you!");
        push.sendInBackground();

    }


    private static class ViewHolder {
        TextView usernameLabel;
        ImageView thumbnail;
        CheckBox checkbox;
    }
}

