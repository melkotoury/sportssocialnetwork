package com.example.sportssocialnetwork.utils;

import android.graphics.Color;

/**
 * Created by melkotoury on 5/12/15.
 */
public class ExtendedColor extends Color {
    public static final int PRIMARY_COLOR= 16733986;
    public static final int PRIMARY_DARK_COLOR= 15092249;
    public static final int ACCENT_COLOR= 240116;
    public static final int PRIMARY_LIGHT= 16764092;
    public static final int PRIMARY_TEXT= 2171169;
    public static final int SECONDARY_TEXT= 7500402;
    public static final int ICONS= 16777215;
    public static final int DIVIDER= 11974326;


   
}
