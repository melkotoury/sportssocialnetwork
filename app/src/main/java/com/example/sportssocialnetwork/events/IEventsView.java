package com.example.sportssocialnetwork.events;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface IEventsView {
    void onDownloadAllEventsSucess();
    void onDownloadAllEventsFailure();
    void onDownloadHostedEventsSucess();
    void onDownloadHostedEventsFailure();


}
