package com.example.sportssocialnetwork.events;

import com.example.sportssocialnetwork.models.EventParse;

import java.util.ArrayList;

/**
 * Created by melkotoury on 5/4/15.
 */
public interface OnRetrieveEventFromParse
{
    public void OnCompletedTask();
}
