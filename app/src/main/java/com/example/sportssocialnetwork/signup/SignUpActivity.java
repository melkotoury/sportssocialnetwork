package com.example.sportssocialnetwork.signup;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.activities.PersonalInfoActivity;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Activity which displays a login screen to the user.
 */
public class SignUpActivity extends AppCompatActivity implements ISignupView,View.OnClickListener{
    // UI references.

    @Bind(R.id.fullname_edit_text)
    EditText fullnameEditText;
    @Bind(R.id.username_edit_text)
    EditText usernameEditText;
    @Bind(R.id.email_edit_text)
    EditText emailEditText;
    @Bind(R.id.password_edit_text)
    EditText passwordEditText;
    @Bind(R.id.password_again_edit_text)
    EditText passwordAgainEditText;
    @Bind(R.id.bsignup_button)
    Button mActionButton;
    @Bind(R.id.app_bar)
    Toolbar toolbar;

  private  String username;
    private String password;
   private  String passwordAgain;
    private String fullname;
   private  String email;

    private ISignupPresenter signupPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        super.setSupportActionBar(toolbar);


        // Set up the submit button click handler
        signupPresenter=new SignupPresenterImpl(this);
        mActionButton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        // Retrieve the text entered from the EditText
        fullname = fullnameEditText.getText().toString().trim();
        username = usernameEditText.getText().toString().trim();
        password = passwordEditText.getText().toString().trim();
        email = emailEditText.getText().toString().trim();

        signupPresenter.validateCredentials(fullname, username, password, email);


    }

    @Override
    public void onIncompleteCredentials() {
        Log.d("LoginSignupActivity", "Please complete the registration form");
        Toast.makeText(SignUpActivity.this, "Please complete registration form", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSignupSucess() {

        Log.d("LoginSignupActivity", "Successfully registered, please log in.");
        Toast.makeText(SignUpActivity.this,
                "Successfully registered, please log in.",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUsernameTaken() {
        Log.d("LoginSignupActivity", "username already taken, please try again.");
        Toast.makeText(SignUpActivity.this,
                "username already taken, please try again",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onInvalidEmail() {
        Log.d("LoginSignupActivity", "invalid email, please try again.");
        Toast.makeText(SignUpActivity.this,
                "invalid email, please try again",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onEmailTaken() {
        Log.d("LoginSignupActivity", "the email address  has already been taken");
        Toast.makeText(SignUpActivity.this,
                "the email address " + email + " has already been taken",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onOtherError(ParseException e) {
        Log.d("LoginSignupActivity", "Sign up Error");
        Toast.makeText(SignUpActivity.this,
                e.getMessage().toString(), Toast.LENGTH_LONG)
                .show();
        e.printStackTrace();
    }


    @Override
    public void navigateToPersonalInfoActivity() {

        startActivity(new Intent(SignUpActivity.this, PersonalInfoActivity.class));
        finish();
    }


}
