package com.example.sportssocialnetwork.events;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;

/**
 * Created by melkotoury on 5/6/15.
 */
public class RetrieveHostingEventListAsyncTask extends AsyncTask<Void, Integer, Void> {

    private static String TABLE_NAME = "Event";

    private ArrayList<EventParse> lisOfEvents;

    private Activity _activity;

    ProgressBar _progressBar;




        public RetrieveHostingEventListAsyncTask(ArrayList < EventParse > eventParses, Activity activity)
        {
            lisOfEvents = eventParses;
            _activity = activity;
        }


        @Override
        public void onPreExecute()
        {
            super.onPreExecute();

        }

        @Override
        public Void doInBackground(Void... obj)
        {
            ParseQuery query = new ParseQuery(TABLE_NAME);
            try
            {
                lisOfEvents.addAll(query.find());

            }
            catch (ParseException e)
            {
            }


            return null;
        }

        @Override
        public void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            ((OnRetrieveEventFromParse)_activity).OnCompletedTask();
        }
    }

