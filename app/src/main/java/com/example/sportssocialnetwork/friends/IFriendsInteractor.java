package com.example.sportssocialnetwork.friends;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 22.11.2015..
 */
public interface IFriendsInteractor {
    void fetchFriendsFromParse( ArrayList<ParseObject> users,ArrayList<ParseObject> userRelations);

}
