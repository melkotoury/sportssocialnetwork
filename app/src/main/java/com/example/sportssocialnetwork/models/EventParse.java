package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by melkotoury on 5/3/15.
 */
@ParseClassName("Event")
public class EventParse extends ParseObject implements Parcelable{
    private String event_title;



    private String event_category;
    private String event_description;
    private String event_location;
    private String event_startDate;
    private String event_startTime;
    private String event_endDate;
    private String event_endTime;
    private boolean is_event_date_passed;
    private String event_hostName;
    private ParseFile event_image;






    // Ensure that your subclass has a public default constructor
public EventParse(){super();}

    // Add a constructor that contains core properties
public EventParse(String event_title,String event_category,String event_description,String event_location
                  ,String event_startDate,String event_startTime,String event_endDate
,String event_endTime,boolean is_event_date_passed,String event_hostName,
                  ParseFile event_image,String event_host_id){
   super();
    setEventTitle(event_title);
    setEvent_category(event_category);
    setEventDescription(event_description);
    setEventLocation(event_location);
    setEventStartDate(event_startDate);
    setEventStartTime(event_startTime);
    setEventEndDate(event_endDate);
    setEventEndTime(event_endTime);
    setIsEventDatePassed(is_event_date_passed);
    setEventHostName(event_hostName);
    setEventImage(event_image);
    setEventHostId(event_host_id);
    
}
//setters to set values to the backend using put



    public void setEvent_category(String event_category) {
        put("event_category",event_category);
    }
    public void setEventHostId(String event_host_id) {
        put("event_host_id",event_host_id);
    }

    public void setEventImage(ParseFile event_image) {
        put("event_image",event_image);

    }

    public void setEventHostName(String event_hostName) {
        put("event_hostName",event_hostName);

    }

    public void setIsEventDatePassed(boolean is_event_date_passed) {
        put("is_event_date_passed",is_event_date_passed);

    }

    public void setEventEndTime(String event_endTime) {
        put("event_endTime",event_endTime);

    }

    public void setEventEndDate(String event_endDate) {
        put("event_endDate",event_endDate);

    }

    public void setEventStartTime(String event_startTime) {
        put("event_startTime",event_startTime);

    }

    public void setEventStartDate(String event_startDate) {
        put("event_startDate",event_startDate);

    }

    public void setEventLocation(String event_location) {
        put("event_location",event_location);

    }

    public void setEventTitle(String event_title) {
        put("event_title",event_title);

    }
   public void setEventDescription(String event_description){
       put("event_description",event_description);

   }
    
    
    
    //use getters to access the fields by getString,getBoolean,getArrayByte

    public String getEvent_category() {
        return getString("event_category");
    }
    public String getEventHostId() {

        return getString("event_host_id");
    }

    public ParseFile getEventImage() {

        return getParseFile("event_image");
    }

    public String getEventHostName() {
    return getString("event_hostName");
    }

    public boolean getIsEventDatePassed() {
    return getBoolean("is_event_date_passed");
    }

    public String getEventEndTime( ) {
    return getString("event_endTime");
    }

    public String getEventEndDate() {
    return getString("event_endDate");
    }

    public String getEventStartTime() {
    return getString("event_startTime");
    }

    public String getEventStartDate() {
    return getString("event_startDate");
    }

    public String getEventLocation() {
    return getString("event_location");
    }

    public String getEventTitle() {
        return getString("event_title");
    }
    public String getEventDescription(){
return getString("event_description");
    }


    //Get the user for this item
    public ParseUser getUser()  {
        return getParseUser("username");
    }

    //Assosciate each ite with a user
    public void setOwner(ParseUser user){
        put("username",user);
    }



    public static final Parcelable.Creator<EventParse> CREATOR
            = new Parcelable.Creator<EventParse>() {
        public EventParse createFromParcel(Parcel in) {
            return new EventParse(in);
        }

        public EventParse[] newArray(int size) {
            return new EventParse[size];
        }
    };


    private EventParse(Parcel in) {
        event_title = in.readString();
        event_category=in.readString();
        event_image= in.readParcelable(getClass().getClassLoader());
        event_description = in.readString();
        event_location = in.readString();
        event_startDate = in.readString();
        event_startTime = in.readString();
        event_endDate = in.readString();
        event_endTime = in.readString();
        is_event_date_passed = in.readByte()!=0;
        event_hostName = in.readString();
    }



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(event_title);
        dest.writeString(event_category);
        dest.writeParcelable((Parcelable) event_image, flags);
        dest.writeString(event_description);
        dest.writeString(event_location);
        dest.writeString(event_startDate);
        dest.writeString(event_startTime);
        dest.writeString(event_endDate);
        dest.writeString(event_endTime);
        dest.writeByte((byte)(is_event_date_passed? 1 : 0));
        dest.writeString(event_hostName);
    }
}
