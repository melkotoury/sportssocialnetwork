package com.example.sportssocialnetwork.callbacks;

import com.example.sportssocialnetwork.models.EventParse;

/**
 * Created by melkotoury on 5/6/15.
 */
public interface HostEventListLoadListener {
    public void onHostEventListLoaded(EventParse eventParse);

}
