package com.example.sportssocialnetwork.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.models.EventParse;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InvitedEventListFragment extends Fragment {


    public InvitedEventListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_invited_event_list, container, false);
    }


}
