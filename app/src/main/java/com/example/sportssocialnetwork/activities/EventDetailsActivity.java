package com.example.sportssocialnetwork.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.main.MainActivity;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

import com.parse.ParseImageView;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventDetailsActivity extends BaseActivity {

    @Bind(R.id.app_bar) Toolbar toolbar;
    @Bind(R.id.event_image) ParseImageView event_image;

    @Bind(R.id.tv_passed_location) TextView tv_passed_location;
    @Bind(R.id.tv_passed_hostname) TextView tv_passed_hostname;
    @Bind(R.id.tv_passed_details) TextView tv_passed_details;
    @Bind(R.id.tv_passed_start_date) TextView tv_passed_start_date;
    @Bind(R.id.tv_passed_start_time) TextView tv_passed_start_time;
    @Bind(R.id.tv_passed_end_date) TextView tv_passed_end_date;
    @Bind(R.id.tv_passed_end_time) TextView tv_passed_end_time;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_details);

        ButterKnife.bind(this);
        setSupportActionBar(toolbar);


        String objectId = getIntent().getStringExtra("objectId");
        ParseQuery<EventParse> query = ParseQuery.getQuery(EventParse.class);
         // First try to find from the cache and only then go to network
        query.setCachePolicy(ParseQuery.CachePolicy.CACHE_ELSE_NETWORK); // or CACHE_ONLY
        // Execute the query to find the object with ID
        query.getInBackground(objectId, new GetCallback<EventParse>() {
            @Override
            public void done(EventParse eventParse, ParseException e) {
                ParseFile photoImage = eventParse.getEventImage();
                photoImage.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        event_image.setImageBitmap(BitmapFactory.decodeByteArray(bytes, 0, bytes.length));
                    }
                });
                tv_passed_location.setText(eventParse.getEventLocation());
                tv_passed_hostname.setText(eventParse.getEventHostName());
                tv_passed_details.setText(eventParse.getEventDescription());
                tv_passed_start_date.setText(eventParse.getEventStartDate());
                tv_passed_start_time.setText(eventParse.getEventStartTime());
                tv_passed_end_date.setText(eventParse.getEventEndDate());
                tv_passed_end_time.setText(eventParse.getEventEndTime());
                setTitle(eventParse.getEventTitle() + "");
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_event_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_back:
                finish();
                break;
            case R.id.action_disclaimer:
                FragmentManager disclaimerManager = getSupportFragmentManager();
                DisclaimerDialogFragment disclaimerDialogFragment = new DisclaimerDialogFragment();
                disclaimerDialogFragment.show(disclaimerManager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(EventDetailsActivity.this, MainActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    }

