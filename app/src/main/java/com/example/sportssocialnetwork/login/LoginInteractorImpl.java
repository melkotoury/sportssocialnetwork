package com.example.sportssocialnetwork.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.utils.GenerateAppHashKey;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Lenovo T420 on 18.11.2015..
 */
public class LoginInteractorImpl implements ILoginInteractor, LogInCallback, GraphRequest.GraphJSONObjectCallback {
    private final List<String> permissions = Arrays.asList("public_profile", "email");
    private ParseUser currentUser;

    private IonLoginFinishedListener mListener;

    public LoginInteractorImpl(IonLoginFinishedListener mListener) {
        this.mListener = mListener;
    }



    @Override
    public void login(final String username, final String password) {

        boolean validationError = false;
        if (username.length() == 0) {
            validationError = true;
            mListener.onUsernameError();
        }
        if (password.length() == 0) {
            if (validationError) {
            }
            validationError = true;
            mListener.onPasswordError();
        }
        if (!validationError) {
            logInWithParse(username, password);
        }
    }

    @Override
    public void logInWithParse(String username, String password) {
        ParseUser.logInInBackground(username, password, this);
    }

    @Override
    public void done(ParseUser parseUser, ParseException e) {
        if (e != null) {
            // Show the error message
            //TODO make error dialog instead of toast message
            Toast.makeText(SportsSocialNetworkApplication.getContext(), e.getMessage(), Toast.LENGTH_LONG).show();
            mListener.onParseloginError(e);
        } else {
            // Start an intent for the timeline activity

            SportsSocialNetworkApplication.updateParseInstallation(parseUser);
            // setResult(RESULT_OK);
            mListener.onLoginSucess();

        }
    }

    @Override
    public void logInWithFacebook(AppCompatActivity activity) {


        //  final List<String> permissions = Arrays.asList("public_profile", "email");
        ParseFacebookUtils.logInWithReadPermissionsInBackground(activity, permissions, new LogInCallback() {
            @Override
            public void done(final ParseUser parseUser, ParseException e) {
                //    progressDialog.dismiss();
                if (parseUser == null) {
                    Log.d(SportsSocialNetworkApplication.TAG, "Uh oh. The user cancelled the Facebook login.");
                } else if (parseUser.isNew()) {
                    //TODO this condition never gets executed. Fix  it!
                    Log.d(SportsSocialNetworkApplication.TAG, "User signed up and logged in through Facebook!");

                    //  linkParseFacebookUser(parseUser);
                    // navigateToHome();
                    currentUser = parseUser;
                    getDetailsFromFacebook();

                } else {
                    Log.d(SportsSocialNetworkApplication.TAG, "User logged in through Facebook!");

                    // linkParseFacebookUser(parseUser);
                    // navigateToHome();
                    currentUser = parseUser;
                    Log.d(SportsSocialNetworkApplication.TAG, "username:!"+currentUser.getUsername());
                    Log.d(SportsSocialNetworkApplication.TAG, "email:" + currentUser.getEmail());
                  //  mListener.onLoginSucess();
                    getDetailsFromFacebook();

                }
            }
        });


    }

    private void getDetailsFromFacebook() {
      //  Log.d(SportsSocialNetworkApplication.TAG, "acess token: " + AccessToken.getCurrentAccessToken().toString() + "______________________________");
        //TODO  AccessToken.getCurrentAccessToken()  returns null  solved
        //D/GraphRequest: Warning: Request without access token missing application ID or client token. solved
        //Sports Social network: graph response: {HttpStatus: 400, errorCode: 2500, errorType: OAuthException, errorMessage: An active access token must be used to query information about the current user.}___
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), this);
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,gender,name");
        request.setParameters(parameters);
        request.executeAsync();
    }

    /*
        *Method extracts user information from facebook account

        */
    @Override
    public void onCompleted(JSONObject jsonObject, GraphResponse graphResponse) {
       //TODO  json object return null , check out what is wrong
        if (jsonObject != null) {
            JSONObject userProfile = new JSONObject();

            try {
                currentUser = ParseUser.getCurrentUser();
                // userProfile.put("facebookId", jsonObject.getLong("id"));

                // userProfile.put("name", jsonObject.getString("name"));
                currentUser.put("fullname", jsonObject.getString("name"));
                if (jsonObject.getString("gender") != null)
                    //  userProfile.put("gender", jsonObject.getString("gender"));

                    if (jsonObject.getString("email") != null)
                        //  userProfile.put("email", jsonObject.getString("email"));
                        currentUser.put("email", jsonObject.getString("email"));
                // Save the user profile info in a user property

                // currentUser.put("profile", userProfile);
                currentUser.saveInBackground();

                // Show the user info
                Log.d(SportsSocialNetworkApplication.TAG, "name: " + jsonObject.get("name") + "______________________________");
                Log.d(SportsSocialNetworkApplication.TAG, "gender: " + jsonObject.get("gender"));
                Log.d(SportsSocialNetworkApplication.TAG, "email: " + jsonObject.get("email"));

                mListener.onLoginSucess();

            } catch (JSONException e) {
                Log.d(SportsSocialNetworkApplication.TAG,
                        "Error parsing returned user data. " + e);
            }
        } else if (graphResponse.getError() != null) {
            Log.d(SportsSocialNetworkApplication.TAG, "graph response: " +graphResponse.getError().toString() + "______________________________");
            switch (graphResponse.getError().getCategory()) {
                case LOGIN_RECOVERABLE:
                    Log.d(SportsSocialNetworkApplication.TAG,
                            "Authentication error: " + graphResponse.getError());
                    break;

                case TRANSIENT:
                    Log.d(SportsSocialNetworkApplication.TAG,
                            "Transient error. Try again. " + graphResponse.getError());
                    break;

                case OTHER:
                    Log.d(SportsSocialNetworkApplication.TAG,
                            "Some other error: " + graphResponse.getError());
                    break;
            }
        }
    }
/*
    private void linkParseFacebookUser(  ParseUser user)
    {

    if (!ParseFacebookUtils.isLinked(user)) {
        ParseFacebookUtils.linkWithReadPermissionsInBackground(user, this, permissions, new SaveCallback() {
            @Override
            public void done(ParseException ex) {
              //  ParseUser s=ParseUser.getCurrentUser();
                if (ParseFacebookUtils.isLinked(user)) {
                    Log.d("MyApp", "Woohoo, user logged in with Facebook!");
                }
            }
        });
    }

    }*/
}
