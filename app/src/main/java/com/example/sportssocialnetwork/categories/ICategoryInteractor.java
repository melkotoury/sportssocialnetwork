package com.example.sportssocialnetwork.categories;

import com.example.sportssocialnetwork.models.CategoryParse;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface ICategoryInteractor {
    void obtainListOfCategories(ArrayList<CategoryParse> listOfCategories);

}
