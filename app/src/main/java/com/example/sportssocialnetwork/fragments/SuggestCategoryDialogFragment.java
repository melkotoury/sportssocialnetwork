package com.example.sportssocialnetwork.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.example.sportssocialnetwork.R;
import com.parse.ParseUser;

public class SuggestCategoryDialogFragment extends DialogFragment {
    Button bSendCategory,bCancelCategory;
    EditText etSuggest;

    public SuggestCategoryDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //dialoge won't be cancled if you pressed outside the dialog
        setCancelable(false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);


        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_suggest_category_dialog, null);
        bSendCategory=(Button)view.findViewById(R.id.bSendCategory);
        bCancelCategory=(Button)view.findViewById(R.id.bCancelCategory);
        etSuggest=(EditText)view.findViewById(R.id.etSuggest);
        bSendCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   if (etSuggest.length()!=0)
                ParseUser currentUser=ParseUser.getCurrentUser();
                Intent sendEmailIntent= new Intent(Intent.ACTION_SEND);
                sendEmailIntent.setData(Uri.parse("mailto:"));
                String [] mailTo={"melkotoury@gmail.com","melkotoury@live.com"};
                sendEmailIntent.putExtra(Intent.EXTRA_EMAIL,mailTo);
                sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT,"Suggested Category");
                sendEmailIntent.putExtra(Intent.EXTRA_TEXT,
                        "Hi My name is " + currentUser.getUsername() + " and I suggest you add a new Category "
                                + etSuggest.getText().toString().trim() + "\n Best Regard,"
                );
                sendEmailIntent.setType("message/rfc822");
               Intent chooser=Intent.createChooser(sendEmailIntent,"Send Email");
                startActivity(chooser);
                dismiss();
            }
        });
        bCancelCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return view;
    }
}


