package com.example.sportssocialnetwork.friends;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class FriendsActivity extends BaseActivity implements IFriendsView{
    public static final String TAG = FriendsActivity.class.getSimpleName();

    private  ArrayList<ParseObject> mUsers=new ArrayList<>();
    private ArrayList<ParseObject> mUserRelations=new ArrayList<>();
    private IFriendsPresenter mPresenter;
    private FriendsListAdapter adapter;

    @Bind(R.id.progressBar1) ProgressBar mProgressBar;
    @Bind(R.id.listview) ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        super.onCreateToolbar();
        ButterKnife.bind(this);

        mPresenter= new FriendsPresenterImpl(this);

        adapter = new FriendsListAdapter(FriendsActivity.this, mUsers, mUserRelations);
        listView.setAdapter(adapter);
        mPresenter.fetchAllUsersFromParse(mUsers, mUserRelations);

    }

    @Override
    public void onDownloadFriendsComplete() {
        adapter.notifyDataSetChanged();
    }
    @Override
    public void onDownloadUsersFailure(ParseException e) {
        Log.d(SportsSocialNetworkApplication.TAG, "onDownloadUsersFailure!: " + e.getLocalizedMessage());
        Toast.makeText(FriendsActivity.this, "Sorry, there was an error getting users!, "+e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void onDownloadFriendsFailure(ParseException e) {
        Log.d(SportsSocialNetworkApplication.TAG, "onDownloadFriendsFailure: " + e.getLocalizedMessage());
        Toast.makeText(FriendsActivity.this, "Sorry, there was an error getting friends!, "+e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
    }
    @Override
    public void onUserHasNoFriends() {
        adapter.notifyDataSetChanged();
        Toast.makeText(FriendsActivity.this, "user has no friends!, ", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }
}
