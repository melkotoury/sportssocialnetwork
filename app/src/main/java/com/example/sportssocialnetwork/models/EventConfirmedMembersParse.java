package com.example.sportssocialnetwork.models;


import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/17/15.
 */
@ParseClassName("EventConfirmedMembers")
public class EventConfirmedMembersParse extends ParseObject implements Parcelable {




    public EventConfirmedMembersParse(){
        super();
    }
    private ParseUser event_confirmed_user_id;
    private EventParse event_id;


    public EventConfirmedMembersParse(ParseUser event_confirmed_user_id,EventParse event_id) {
    addConfirmedMemberToEvent(event_confirmed_user_id);
        setEventAssociated(event_id);
    }




    // Associate member with Event
    public void addConfirmedMemberToEvent(ParseUser user) {
        put("event_confirmed_user_id", user);
    }

    // Get the confirmed user for this event
    public ParseUser getConfirmedEventMember()  {
        return getParseUser("event_confirmed_user_id");
    }



    // Associate each member with an Event
    public void setEventAssociated(EventParse eventParse) {
        put("event_id", eventParse);
    }

    // Get the Event for this confirmed Member
    public EventParse getEventforThisConfirmedMember()  {
        return (EventParse) getParseObject("event_id");
    }



    public  final Parcelable.Creator<EventConfirmedMembersParse> CREATOR
            = new Parcelable.Creator<EventConfirmedMembersParse>() {
        public EventConfirmedMembersParse createFromParcel(Parcel in) {
            return new EventConfirmedMembersParse(in);
        }

        public EventConfirmedMembersParse[] newArray(int size) {
            return new EventConfirmedMembersParse[size];
        }
    };

    public EventConfirmedMembersParse(Parcel in) {
        event_confirmed_user_id =(ParseUser) in.readParcelable(ParseUser.class.getClassLoader());
        event_id = (EventParse)in.readParcelable(EventParse.class.getClassLoader());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) event_confirmed_user_id, flags);
        dest.writeParcelable( event_id, flags);
    }
    
}
