package com.example.sportssocialnetwork.signup;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface ISignupPresenter {
    void validateCredentials(String fullName,String username,String password,String email);
}
