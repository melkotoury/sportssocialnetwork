package com.example.sportssocialnetwork.signup;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.example.sportssocialnetwork.activities.PersonalInfoActivity;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class SignupInteractorImpl implements ISignupInteractor, SignUpCallback {

    private String fullName;
    private String username;
    private String password;
    private String email;

    private IonSignupFinishedListener mListener;

    public SignupInteractorImpl(IonSignupFinishedListener listener) {
        mListener = listener;
    }
    @Override
    public void validateCredentials(String fullName, String username, String password, String email) {
        this.fullName = fullName;
        this.username = username;
        this.password = password;
        this.email = email;

        // Force user to fill up the form
        if (username.equals("") || password.equals("") || email.equals("")) {
            mListener.OnIncompleteCredentials();

        } else {
            signUpWithParse();
        }
    }
    @Override
    public void signUpWithParse() {
        // Save new user data into Parse.com Data Storage
        ParseUser user = new ParseUser();
        user.put("fullname", fullName);
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        user.signUpInBackground(this);
    }
    @Override
    public void done(ParseException e) {
        if (e == null) {
            SportsSocialNetworkApplication.updateParseInstallation(ParseUser.getCurrentUser());
            mListener.onSignupSucess();
            // Show a simple Toast message upon successful registration
        } else {
            mListener.onSignupFail(e);
        }
    }
}
