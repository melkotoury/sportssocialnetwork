package com.example.sportssocialnetwork.adapters;


import android.app.Activity;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.activities.EventDetailsActivity;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

import java.util.ArrayList;


public class AllEventListAdapter extends RecyclerView.Adapter<AllEventListAdapter.ViewHolder> {


    private ArrayList<EventParse> listOfEvents;
    private Activity _activity;

    int positionOfCard;
    public AllEventListAdapter(ArrayList<EventParse> eventParses, Activity activity) {
        listOfEvents = eventParses;
        _activity = activity;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener,AdapterView.OnItemClickListener {
        // each data item is just a string in this case
        public TextView tvCVEventTitle;
        public ImageView ivCVEventImage;
        public TextView tvCVEventDate;
        public TextView tvCVEventHostName;
        private Button bCVGoing,bCVMaybe,bCVDecline;


        public ViewHolder(View v) {
            super(v);

            tvCVEventTitle = (TextView) v.findViewById(R.id.tvCVEventTitle);
            ivCVEventImage = (ImageView) v.findViewById(R.id.ivCVEventImage);
            tvCVEventDate = (TextView) v.findViewById(R.id.tvCVEventDate);
            tvCVEventHostName=(TextView) v.findViewById(R.id.tvCVEventHostName);

            bCVGoing = (Button) v.findViewById(R.id.bCVGoing);
            bCVMaybe = (Button) v.findViewById(R.id.bCVMaybe);
            bCVDecline = (Button) v.findViewById(R.id.bCVDecline);
            v.setOnCreateContextMenuListener(this);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   int position=getPosition();
                    Intent intent=new Intent(_activity, EventDetailsActivity.class);
                    String objectId=listOfEvents.get(position).getObjectId();
                    intent.putExtra("objectId",objectId);
                    _activity.startActivity(intent);

                }
            });


        }


        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.setHeaderTitle("Select The Action");
            menu.add(0, v.getId(), 0, "Edit");//groupId, itemId, order, title
            menu.add(0, v.getId(), 0, "Delete");

        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }


    public void notifyEventParseAdded() {
        notifyDataSetChanged();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public AllEventListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {



        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.event_list_recycleout, parent, false);

        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

        EventParse currentEvent=listOfEvents.get(position);
        positionOfCard=position;

        //Access data using the get methods for the object
        holder.tvCVEventTitle.setText(currentEvent.getEventTitle());
        holder.tvCVEventDate.setText(currentEvent.getEventStartDate());
        ParseFile photoImage=currentEvent.getEventImage();
        photoImage.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                holder.ivCVEventImage.setImageBitmap(BitmapFactory.decodeByteArray(bytes,
                        0, bytes.length));
            }
        });

        holder.tvCVEventHostName.setText(currentEvent.getEventHostName());


        holder.bCVGoing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        holder.bCVMaybe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.bCVDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if (listOfEvents != null) {
            return listOfEvents.size();
        } else
            return 0;
    }

    public void setlistOfEvents(ArrayList<EventParse> listOfEvents) {
        this.listOfEvents = listOfEvents;
    }


}


