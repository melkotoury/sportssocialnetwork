package com.example.sportssocialnetwork.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by melkotoury on 5/17/15.
 */
@ParseClassName("EventInvitedMembers")
public class EventInvitedMembersParse extends ParseObject implements Parcelable {




    public EventInvitedMembersParse(){
        super();
    }
    private ParseUser event_invited_user_id;
    private EventParse event_id;


    public EventInvitedMembersParse(ParseUser event_invited_user_id,EventParse event_id) {
        addInvitedMemberToEvent(event_invited_user_id);
        setEventAssociated(event_id);
    }




    // Associate member with Event
    public void addInvitedMemberToEvent(ParseUser user) {
        put("event_invited_user_id", user);
    }

    // Get the confirmed user for this event
    public ParseUser getInvitedEventMember()  {
        return getParseUser("event_invited_user_id");
    }



    // Associate each member with an Event
    public void setEventAssociated(EventParse eventParse) {
        put("event_id", eventParse);
    }

    // Get the Event for this Invited Member
    public EventParse getEventforThisInvitedMember()  {
        return (EventParse) getParseObject("event_id");
    }



    public  final Parcelable.Creator<EventInvitedMembersParse> CREATOR
            = new Parcelable.Creator<EventInvitedMembersParse>() {
        public EventInvitedMembersParse createFromParcel(Parcel in) {
            return new EventInvitedMembersParse(in);
        }

        public EventInvitedMembersParse[] newArray(int size) {
            return new EventInvitedMembersParse[size];
        }
    };

    public EventInvitedMembersParse(Parcel in) {
        event_invited_user_id =(ParseUser) in.readParcelable(ParseUser.class.getClassLoader());
        event_id = (EventParse)in.readParcelable(EventParse.class.getClassLoader());
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) event_invited_user_id, flags);
        dest.writeParcelable( event_id, flags);
    }

}
