package com.example.sportssocialnetwork.categories;

import com.example.sportssocialnetwork.models.CategoryParse;
import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public class CategoryPresenterImpl implements ICategoryPresenter,IonDownloadCategoriesCompleteListener{

    private ICategoryView mCategoryView;
    private ICategoryInteractor mCategoryInteractor;

    public CategoryPresenterImpl(ICategoryView categoryView) {
        mCategoryView=categoryView;
        mCategoryInteractor= new CategoryInteractorImpl(this);

    }
//method from ICategoryPresenter
    @Override
    public void downloadListOfCategories(ArrayList<CategoryParse> listOfCategories) {
        mCategoryInteractor.obtainListOfCategories(listOfCategories);
    }

    //methods from IonDownloadCategoriesCompleteListener
    @Override
    public void onDownloadCategoriesSucess() {
        mCategoryView.onDownloadCategoriesSucess();
    }

    @Override
    public void onDownloadCategoriesFailure(ParseException e) {
        mCategoryView.onDownloadCategoriesError(e);
    }
}
