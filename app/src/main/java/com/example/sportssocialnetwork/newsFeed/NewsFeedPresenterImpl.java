package com.example.sportssocialnetwork.newsFeed;

import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseException;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 21.11.2015..
 */
public class NewsFeedPresenterImpl implements INewsFeedPresenter, IonDownloadNewsFeedListFinishedListener {

    private INewsFeedView newsFeedView;
    private INewsFeedInteractor newsFeedInteractor;


    public NewsFeedPresenterImpl(INewsFeedView newsFeedView) {
        this.newsFeedView = newsFeedView;
        this.newsFeedInteractor = new NewsFeedInteractorImpl(this);
    }


    @Override
    public void downloadNewsFeedList(ArrayList<EventParse> listOfEvents) {
        newsFeedInteractor.obtainNewsFeedList(listOfEvents);

    }
    @Override
    public void onDownloadNewsFeedListSucess() {
        newsFeedView.onDownloadNewsComplete();
    }

    @Override
    public void onDownloadNewsFeedListFailure(ParseException e) {
        newsFeedView.onDownloadNewsError(e);
    }


}
