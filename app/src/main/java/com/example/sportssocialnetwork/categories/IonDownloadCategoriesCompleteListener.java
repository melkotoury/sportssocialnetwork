package com.example.sportssocialnetwork.categories;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 25.11.2015..
 */
public interface IonDownloadCategoriesCompleteListener {

    void onDownloadCategoriesSucess();
    void onDownloadCategoriesFailure(ParseException e);
}
