package com.example.sportssocialnetwork.categories;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.activities.EventsInCategory;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;


public class CategoryListAdapter extends RecyclerView.Adapter<CategoryListAdapter.ViewHolder>  {
    private Context context;


    public CategoryListAdapter(Context context){
        this.context=context;
    }


    private ArrayList<CategoryParse> listOfCategories;
    private Activity _activity;


    public CategoryListAdapter(ArrayList<CategoryParse> categories, Activity activity) {
        listOfCategories = categories;
        _activity = activity;
    }
    @Override
    public CategoryListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.category_list_card_view, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    private void test(){}

    @Override
    public void onBindViewHolder(final CategoryListAdapter.ViewHolder holder, final int position) {
  /*  holder.cvCivCategoryImage.setImageBitmap(BitmapFactory.decodeByteArray(listOfCategories.get(position).getCategoryImage(),
            0, listOfCategories.get(position).getCategoryImage().length));
            */
        ParseFile photoFile=listOfCategories.get(position).getCategoryImage();
        photoFile.getDataInBackground(new GetDataCallback() {
            @Override
            public void done(byte[] bytes, ParseException e) {
                holder.cvCivCategoryImage.setImageBitmap(BitmapFactory.decodeByteArray(bytes,
                        0, bytes.length));
            }
        });
        holder.cvTvCategoryName.setText(listOfCategories.get(position).getCategoryName());
        holder.cvTvCountCategoryFollowers.setText(String.valueOf(listOfCategories.get(position).getCategoryCountMembers()));
        holder.cvTbFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    holder.cvTvCountCategoryFollowers.setText((listOfCategories.get(position).getCategoryCountMembers() + 1) + "");
                    holder.cvTbFollow.setBackgroundColor(_activity.getResources().getColor(R.color.primaryColor));
                    holder.cvTbFollow.setTextColor(_activity.getResources().getColor(R.color.white));
                    holder.cvTbFollow.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(_activity, (R.drawable.ic_action_friend_added)), null, null, null);
                    final int count=Integer.parseInt(holder.cvTvCountCategoryFollowers.getText().toString());
                    String id= listOfCategories.get(position).getObjectId();
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");

// Retrieve the object by id
                    query.getInBackground(id, new GetCallback<ParseObject>(){
                        public void done(ParseObject categoryParse, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                categoryParse.put("category_count_members", count);
                                categoryParse.saveEventually(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e==null) {
                                            Toast.makeText(_activity.getBaseContext(), "After Saving (Add) " + listOfCategories.get(position).getCategoryCountMembers() + "", Toast.LENGTH_LONG);
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
                else {
                    holder.cvTvCountCategoryFollowers.setText((listOfCategories.get(position).getCategoryCountMembers() - 1) + "");
                    Toast.makeText(_activity.getBaseContext(),listOfCategories.get(position).getCategoryCountMembers()+"",Toast.LENGTH_LONG).show();
                    holder.cvTbFollow.setBackgroundColor(_activity.getResources().getColor(R.color.white));
                    holder.cvTbFollow.setTextColor(_activity.getResources().getColor(R.color.primaryColor));
                    holder.cvTbFollow.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(_activity, (R.drawable.ic_action_add_friend)),null,null,null);
                     final int count=Integer.parseInt(holder.cvTvCountCategoryFollowers.getText().toString());
                    String id= listOfCategories.get(position).getObjectId();
                    ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");

// Retrieve the object by id
                    query.getInBackground(id, new GetCallback<ParseObject>() {
                        public void done(ParseObject categoryParse, ParseException e) {
                            if (e == null) {
                                // Now let's update it with some new data. In this case, only cheatMode and score
                                // will get sent to the Parse Cloud. playerName hasn't changed.
                                categoryParse.put("category_count_members", count);
                                categoryParse.saveEventually(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if (e== null){
                                        }
                                    }
                                });
                            }
                        }
                    });

                }
            }
        });


    }

    @Override
    public int getItemCount() {
        if (listOfCategories != null) {
            return listOfCategories.size();
        } else
            return 0;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView cvCivCategoryImage;
        TextView cvTvCategoryName;
        ToggleButton cvTbFollow;
        TextView cvTvCountCategoryFollowers;
        public ViewHolder(View v) {
            super(v);
            cvCivCategoryImage=(CircleImageView)v.findViewById(R.id.cvCivCategoryImage);
            cvTvCategoryName=(TextView)v.findViewById(R.id.cvTvCategoryName);
            cvTvCountCategoryFollowers=(TextView)v.findViewById(R.id.cvTvCountCategoryFollowers);
            cvTbFollow=(ToggleButton)v.findViewById(R.id.cvTbFollow);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(_activity.getBaseContext(),EventsInCategory.class);
                    String categoryName=listOfCategories.get(getPosition()).getCategoryName();
                    intent.putExtra("Category",categoryName);
                    _activity.startActivity(intent);
                }
            });
        }
    }
    public void setListofCategories(ArrayList<CategoryParse> listOfCategories) {
        this.listOfCategories = listOfCategories;
    }
}




