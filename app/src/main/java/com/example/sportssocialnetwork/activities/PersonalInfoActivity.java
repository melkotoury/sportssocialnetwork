/*
 * Copyright (C) 2015 Google Inc. All Rights Reserved.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.example.sportssocialnetwork.activities;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.adapters.PlaceAutocompleteAdapter;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.main.MainActivity;
import com.example.sportssocialnetwork.models.PersonParse;
import com.example.sportssocialnetwork.signup.SignUpActivity;
import com.example.sportssocialnetwork.utils.Util;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PersonalInfoActivity extends AppCompatActivity

        implements GoogleApiClient.OnConnectionFailedListener {

    /**
     * GoogleApiClient wraps our service connection to Google Play Services and provides access
     * to the user's sign in state as well as the Google's APIs.
     */
    protected GoogleApiClient mGoogleApiClient;
    private PlaceAutocompleteAdapter mAdapter;

    private ParseFile photoFile;


    @Bind(R.id.acet_person_address)AutoCompleteTextView acet_person_address;
    @Bind(R.id.tvAddPersonImage)TextView tvAddPersonImage;
    @Bind(R.id.ivPersonImage) ImageView ivPersonImage;
    @Bind(R.id.spPersonGender)Spinner spPersonGender;
    @Bind(R.id.etPersonDetails)EditText etPersonDetails;
    @Bind(R.id.etPersonBirthdate)EditText etPersonBirthdate;
    @Bind(R.id.app_bar)Toolbar toolbar;




    Bitmap bitmapUserImage;
    private static int IMAGE_PICK_REQUEST = 101;
    final Calendar c = Calendar.getInstance();



    private static final LatLngBounds BOUNDS_GREATER_SYDNEY = new LatLngBounds(
            new LatLng(-34.041458, 150.790100), new LatLng(-33.682247, 151.383362));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Construct a GoogleApiClient for the {@link Places#GEO_DATA_API} using AutoManage
        // functionality, which automatically sets up the API client to handle Activity lifecycle
        // events. If your activity does not extend FragmentActivity, make sure to call connect()
        // and disconnect() explicitly.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addApi(Places.GEO_DATA_API)
                .build();

        setContentView(R.layout.activity_personal_info);
        //Locate Toolbar
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        // Retrieve the AutoCompleteTextView that will display Place suggestions.

        // Register a listener that receives callbacks when a suggestion has been selected
        acet_person_address.setOnItemClickListener(mAutocompleteClickListener);

        // Set up the adapter that will retrieve suggestions from the Places Geo Data API that cover
        // the entire world.
        mAdapter = new PlaceAutocompleteAdapter(this, android.R.layout.simple_list_item_1,
                mGoogleApiClient, BOUNDS_GREATER_SYDNEY, null);
        acet_person_address.setAdapter(mAdapter);

        //Listeners
        tvAddPersonImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, IMAGE_PICK_REQUEST);
            }
        });
        etPersonBirthdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(PersonalInfoActivity.this, birthdate, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    DatePickerDialog.OnDateSetListener birthdate = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            c.set(Calendar.YEAR, year);
            c.set(Calendar.MONTH, monthOfYear);
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            setCurrentDateOnView();
        }
    };
    public void setCurrentDateOnView() {
        String dateFormat = "MM-dd-yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.US);
        etPersonBirthdate.setText(sdf.format(c.getTime()));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        if (resultCode == RESULT_OK)
        {
            if (requestCode == IMAGE_PICK_REQUEST)
            {
                String filePathMame = Util.getRealPathFromURI(imageReturnedIntent.getData(), this);
                BitmapFactory.Options options = new BitmapFactory.Options();
                bitmapUserImage = (Bitmap) BitmapFactory.decodeFile(filePathMame, options);
                bitmapUserImage=Bitmap.createScaledBitmap(bitmapUserImage,200, 200
                        * bitmapUserImage.getHeight() / bitmapUserImage.getWidth(), false);
                ivPersonImage.setImageBitmap(bitmapUserImage);
            }

        }
    }
    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a PlaceAutocomplete object from which we
             read the place ID.
              */
            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
          //  Log.i(TAG, "Autocomplete item selected: " + item.description);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

            Toast.makeText(getApplicationContext(), "Clicked: " + item.description,
                    Toast.LENGTH_SHORT).show();
          //  Log.i(TAG, "Called getPlaceById to get Place details for " + item.placeId);
        }
    };

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
            //    Log.e(TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);

           // Log.i(TAG, "Place details received: " + place.getName());

            places.release();
        }
    };

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

       // Log.e(TAG, "onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());

        // TODO(Developer): Check error code and notify the user of error state and resolution.
        Toast.makeText(this,
                "Could not connect to Google API Client: Error " + connectionResult.getErrorCode(),
                Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_personal_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {
            case R.id.action_confirm:

                if (bitmapUserImage == null) {
                    bitmapUserImage = BitmapFactory.decodeResource(getResources(), R.drawable.ic_avatar);
                }
                byte[] user_image = Util.BitmapToByteArray(bitmapUserImage);
                final ParseFile photoImage=new ParseFile(user_image);
                photoImage.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        // Set up a progress dialog
                        /*
                        final ProgressDialog dialog = new ProgressDialog(PersonalInfoActivity.this);
                        dialog.setMessage(getString(R.string.progress_saving_personal));
                        dialog.show();
                        */
                        if (e == null) {
                            ParseUser user = ParseUser.getCurrentUser();
                            // String user_id=user.getObjectId();
                            String fullname = user.getString("fullname");
                            String username = user.getUsername();
                            final String email = user.getEmail();
                            String gender = spPersonGender.getSelectedItem().toString().trim();
                            String psersonDetails = etPersonDetails.getText().toString().trim();
                            String address = acet_person_address.getText().toString().trim();
                            Date birthdate = c.getTime();
                            // Save new user data into Parse.com Data Storage
                            PersonParse personParse = new PersonParse(user, fullname, username, email, birthdate, address, photoImage, gender, psersonDetails);
                            personParse.saveEventually(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        Toast.makeText(PersonalInfoActivity.this, "Successfully Saved your Data", Toast.LENGTH_LONG).show();
                                        startActivity(new Intent(PersonalInfoActivity.this, MainActivity.class));
                                    } else {
                                        Toast.makeText(PersonalInfoActivity.this, e.getMessage().toString(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        } else {

                        }
                    }
                });



                finish();
                break;
            case R.id.action_cancel:
                startActivity(new Intent(PersonalInfoActivity.this,SignUpActivity.class));
                finish();
                break;
            case R.id.action_settings:

                break;

            case R.id.action_disclaimer:
                FragmentManager manager = getSupportFragmentManager();
                DisclaimerDialogFragment dialogFragment = new DisclaimerDialogFragment();
                dialogFragment.show(manager, "MyDialog");
                break;
            case R.id.action_logout:
                ParseUser.logOut();
                startActivity(new Intent(this, MainActivity.class));
                break;
        }

        return super.onOptionsItemSelected(item);
    }




}
