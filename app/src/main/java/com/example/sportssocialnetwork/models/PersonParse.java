package com.example.sportssocialnetwork.models;

import java.io.Serializable;
import java.util.Date;


import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.parse.GetDataCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

@ParseClassName("Person")
public class PersonParse extends ParseObject implements Parcelable {




    ParseUser user_id;
     String fullname;
     String username;
    Date birthdate;
    String address;
    ParseFile user_image;
     String email;
    String gender;
    String personDetails;


    public PersonParse(){
        super();
    }

    public PersonParse(ParseUser user_id,String fullname, String username,  String email, Date birthdate, String address, ParseFile user_image, String gender,String personDetails){
       setUser_id(user_id);
        setfullname(fullname);
        setUsername(username);
        setEmail(email);
        setbirthdate(birthdate);
        setAddress(address);
        setUser_image(user_image);
        setGender(gender);
        setPersonDetails(personDetails);
    }

    public void setUser_id(ParseUser user_id) {
        put("user_id", user_id);
    }
    public ParseUser getUser_id(){
        return getParseUser("user_id");
    }
    public String getUsername() {
        return getString("username");
    }
    public void setUsername(String username) {
        put("username",username);
    }

    public String getfullname() {
        return getString("fullname");
    }
    public void setfullname(String fullname) {
        put("fullname",fullname);
    }

    public Date getbirthdate() {
        return getDate("birthdate");
    }
    public void setbirthdate(Date birthdate) {
        put("birthdate",birthdate);
    }
    public String getAddress() {
        return getString("address");
    }
    public void setAddress(String address) {
        this.address = address;
        put("address", address);
    }

    public ParseFile getUser_Image() {

         return getParseFile("user_image");

    }
    public void setUser_image(ParseFile user_image) {


        put("user_image", user_image);
    }
    public String getEmail() {
return getString("email");
    }
    public void setEmail(String email) {
        this.email = email;
        put("email",email);
    }

    public String getGender() {
        return getString("gender");
    }
    public void setGender(String gender) {
        put("gender",gender);
    }

    public void setPersonDetails(String personDetails) {
        put("personDetails",personDetails);
    }
    public String getPersonDetails(){
        return getString(personDetails);
    }

    public  final Parcelable.Creator<PersonParse> CREATOR
            = new Parcelable.Creator<PersonParse>() {
        public PersonParse createFromParcel(Parcel in) {
            return new PersonParse(in);
        }

        public PersonParse[] newArray(int size) {
            return new PersonParse[size];
        }
    };


    private PersonParse(Parcel in) {

        user_id = in.readParcelable(getClass().getClassLoader());
        fullname=in.readString();
        username=in.readString();
        email=in.readString();
		birthdate=new Date(in.readLong());
		address=in.readString();
		//user_image = new byte[in.readInt()];
		//in.readByteArray(user_image);
        user_image = in.readParcelable(getClass().getClassLoader());
        gender=in.readString();
		personDetails=in.readString();

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable) user_id, flags);
        dest.writeString(fullname);
        dest.writeString(username);
        dest.writeString(email);
        dest.writeLong(birthdate.getTime());
		dest.writeString(address);
		//dest.writeInt(user_image.length);
		//dest.writeByteArray(user_image);
        dest.writeParcelable((Parcelable) user_image, flags);

        dest.writeString(gender);
        dest.writeString(personDetails);

    }



}
