package com.example.sportssocialnetwork.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.sportssocialnetwork.newsFeed.NewsFeedActivity;
import com.example.sportssocialnetwork.activities.WelcomeActivity;

/**
 * Importing appcompat and supportLibraries
 */

public class MainActivity extends AppCompatActivity implements IMainView {
    private IMainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainPresenter = new MainPresenterImpl(this);
        mainPresenter.checkIfUserIsLoggedIn();
    }

    @Override
    public void onResume() {
        super.onResume();

        mainPresenter.checkIfUserIsLoggedIn();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void navigateToNewsFeedActivity() {

        startActivity(new Intent(MainActivity.this, NewsFeedActivity.class));
        finish();
    }

    @Override
    public void navigateToWelcomeActivity() {
        startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
        finish();
    }
}