package com.example.sportssocialnetwork.friends;

import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 22.11.2015..
 */
public class FriendsPresenterImpl implements IFriendsPresenter, IonDownloadFriendsCompleteListener {
    private IFriendsInteractor mFriendsInteractor;
    private IFriendsView mFriendsView;

    public FriendsPresenterImpl(IFriendsView view) {
        mFriendsView = view;
        mFriendsInteractor = new FriendsInteractorImpl(this);

    }

    //method from IFriendsPresenter
   @Override
     public void fetchAllUsersFromParse( ArrayList<ParseObject> users,ArrayList<ParseObject> userRelations) {
        mFriendsView.showProgress();
        mFriendsInteractor.fetchFriendsFromParse(users,userRelations);

    }


    // methods from IonDownloadFriendsCompleteListener

    @Override
    public void onDownloadUsersFailure(ParseException e) {
        mFriendsView.hideProgress();
mFriendsView.onDownloadUsersFailure(e);
    }
    @Override
    public void onDownloadFriendsSucess() {

        mFriendsView.hideProgress();
        mFriendsView.onDownloadFriendsComplete();
    }
    @Override
    public void onDownloadFriendsFailure(ParseException e) {
        mFriendsView.hideProgress();
        mFriendsView.onDownloadFriendsFailure(e);
    }

    @Override
    public void onUserHasNoFriends() {
        mFriendsView.hideProgress();
mFriendsView.onUserHasNoFriends();
    }


}
