package com.example.sportssocialnetwork.asyncTasks;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ProgressBar;

import com.example.sportssocialnetwork.events.OnRetrieveEventFromParse;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseException;
import com.parse.ParseQuery;

import java.util.ArrayList;

/**
 * Created by melkotoury on 6/12/15.
 */
public class RetrieveEventsListInCategoryAsyncTask
        extends AsyncTask<Void, Integer, Void>
{

    private static String TABLE_NAME = "Event";

    private ArrayList<EventParse> lisOfEvents;

    private Activity _activity;
    String categoryInEvent;

    ProgressBar _progressBar;

    public RetrieveEventsListInCategoryAsyncTask(ArrayList<EventParse> eventParses, Activity activity,String categoryName)
    {
        lisOfEvents = eventParses;
        _activity = activity;
        categoryInEvent=categoryName;
    }


    @Override
    public void onPreExecute()
    {
        super.onPreExecute();

    }

    @Override
    public Void doInBackground(Void... obj)
    {
        ParseQuery query = new ParseQuery(TABLE_NAME);
        query.whereEqualTo("event_category",categoryInEvent);
        try
        {
            lisOfEvents.addAll(query.find());

        }
        catch (ParseException e)
        {
        }

        return null;
    }

    @Override
    public void onPostExecute(Void result)
    {
        super.onPostExecute(result);
        ((OnRetrieveEventFromParse)_activity).OnCompletedTask();
    }
}