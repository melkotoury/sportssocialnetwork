package com.example.sportssocialnetwork.adapters;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.sportssocialnetwork.R;
//import com.example.sportssocialnetwork.activities.TimelineActivity;
import com.example.sportssocialnetwork.models.PersonParse;
//import com.example.sportssocialnetwork.models.PostParse;
import com.example.sportssocialnetwork.utils.DateTimeUtils;
import com.parse.FindCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.net.ssl.SSLKeyException;


public class TimelineAdapter extends RecyclerView.Adapter<TimelineAdapter.ViewHolder> {


  //  private ArrayList<PostParse> listofPosts;
    private Activity _activity;

    /*public TimelineAdapter(ArrayList<PostParse> posts, Activity activity) {
        listofPosts = posts;
        _activity = activity;
    }*/

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvUserName;
        public ImageView imuserPicture;
        public TextView tvPost;
        public TextView tvTimeSincePostCreated;

        private ToggleButton bLike;
        private Button bComment;
        private Button bShare;
   ;


        public ViewHolder(View v) {
            super(v);

            tvUserName = (TextView) v.findViewById(R.id.cardview_username);
            imuserPicture = (ImageView) v.findViewById(R.id.cardview_user_picture);
            tvPost = (TextView) v.findViewById(R.id.carview_post);
            tvTimeSincePostCreated = (TextView) v.findViewById(R.id.cardview_timesincepostcreated);

            bLike = (ToggleButton) v.findViewById(R.id.button_like);
            bComment = (Button) v.findViewById(R.id.button_comment);
            bShare = (Button) v.findViewById(R.id.button_share);

        }
    }


    public void notifyPostAdded() {
        notifyDataSetChanged();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public TimelineAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview_layout, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element

       // ParseUser user=listofPosts.get(position).get_post_user_id();
        ParseQuery<PersonParse> query=ParseQuery.getQuery(PersonParse.class);
      //  query.whereEqualTo("user_id", user);
        query.findInBackground(new FindCallback<PersonParse>() {
            @Override
            public void done(List<PersonParse> list, ParseException e) {
                if (e == null) {
                    String fullname = list.get(0).getfullname();
                    ParseFile photoImage=list.get(0).getUser_Image();
                    photoImage.getDataInBackground(new GetDataCallback() {
                        @Override
                        public void done(byte[] bytes, ParseException e) {
                            holder.imuserPicture.setImageBitmap(BitmapFactory.decodeByteArray(bytes,
                                    0, bytes.length));
                        }
                    });
                    holder.tvUserName.setText(fullname);
             //       holder.tvPost.setText(listofPosts.get(position).get_post_message());
                  /*  holder.tvTimeSincePostCreated.setText
                            (DateTimeUtils.timeSinceCreated(listofPosts.get(position).get_post_time_of_creation()));*/
                } else {
                    Toast.makeText(_activity.getBaseContext(), e.getMessage().toString(), Toast.LENGTH_LONG).show();
                /*    holder.tvUserName.setText(listofPosts.get(position).get_post_user_id().getUsername());
                    holder.tvPost.setText(listofPosts.get(position).get_post_message());
                    holder.tvTimeSincePostCreated.setText
                            (DateTimeUtils.timeSinceCreated(listofPosts.get(position).get_post_time_of_creation()));*/
                }
            }
        });


        /*holder.imuserPicture.setImageBitmap(BitmapFactory.decodeByteArray(listofPosts.get(position).getImage(),
                0, listofPosts.get(position).getImage().length));*/


        holder.bLike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                }
                else {

                }
            }
        });

        holder.bComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        holder.bShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      /*  if (listofPosts != null) {
            return listofPosts.size();
       } else  */
            return 0;
    }

   /* public void setListofPosts(ArrayList<PostParse> listofPosts) {
        this.listofPosts = listofPosts;*/
  //  }


}