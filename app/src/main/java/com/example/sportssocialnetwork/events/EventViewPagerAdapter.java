package com.example.sportssocialnetwork.events;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sportssocialnetwork.fragments.AllEventListFragment;
import com.example.sportssocialnetwork.fragments.HostingEventListFragment;
import com.example.sportssocialnetwork.fragments.InvitedEventListFragment;


public class EventViewPagerAdapter extends FragmentStatePagerAdapter{
    public EventViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        if (position==0){
            fragment=new AllEventListFragment();
        }
        if (position==1){
            fragment=new HostingEventListFragment();

        }
        if (position==2){
            fragment=new InvitedEventListFragment();

        }
        return fragment;
    }

    private final String[] TITLES = { "All", "Hosting", "Invited" };



    @Override
    public CharSequence getPageTitle(int position) {
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }



}

