package com.example.sportssocialnetwork.callbacks;

import com.example.sportssocialnetwork.models.EventParse;

import java.util.ArrayList;

public interface OnRetreivInvitedEvents
{
    public ArrayList<EventParse> onRetrieveInvitedEventsList();
}