package com.example.sportssocialnetwork.friends;

import com.parse.ParseException;

/**
 * Created by Lenovo T420 on 22.11.2015..
 */
public interface IonDownloadFriendsCompleteListener {
    void onDownloadUsersFailure(ParseException e);

    void onDownloadFriendsSucess();

    void onDownloadFriendsFailure(ParseException e);

    void onUserHasNoFriends();

}
