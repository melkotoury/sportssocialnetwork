package com.example.sportssocialnetwork.newsFeed;

import com.example.sportssocialnetwork.models.EventParse;

import java.util.ArrayList;

/**
 * Created by Lenovo T420 on 21.11.2015..
 */
public interface INewsFeedInteractor {
    void obtainNewsFeedList(ArrayList<EventParse> listOfEvents);
}
