package com.example.sportssocialnetwork.application;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.models.CategoryMembersParse;
//import com.example.sportssocialnetwork.models.CategoryMentionsParse;
import com.example.sportssocialnetwork.models.CategoryParse;
import com.example.sportssocialnetwork.models.EventConfirmedMembersParse;
import com.example.sportssocialnetwork.models.EventDeclinedMembersParse;
import com.example.sportssocialnetwork.models.EventInvitedMembersParse;
import com.example.sportssocialnetwork.models.EventMaybeMembersParse;
import com.example.sportssocialnetwork.models.EventParse;
import com.example.sportssocialnetwork.models.PersonParse;
//import com.example.sportssocialnetwork.models.PostParse;
import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by Nikola on 2.4.2015..
 */
public class SportsSocialNetworkApplication extends Application {
    public static final String TAG = "Sports Social network";
    private static SportsSocialNetworkApplication sInstance;

    public static final String KEY_USER_ID = "userId";
    private String parse_app_id;
    private String parse_client_key;
    private String parse_channel;
  //  private static DBAllEventList mDatabaseAllEventList;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext=this;


        // Add your initialization code here

       // GenerateAppHashKey key = new GenerateAppHashKey(getPackageManager());


        parse_app_id = getResources().getString(R.string.parse_app_id);
        parse_client_key = getResources().getString(R.string.parse_client_key);
        parse_channel="SportsSocialNetworkChannel";
        //  Parse.initialize(this, APPLICATION_ID, CLIENT_KEY);

        /*

        sInstance=this;
        mDatabaseAllEventList=new DBAllEventList(this);
*/
        //Register Parse Models
        ParseObject.registerSubclass(EventParse.class);
        ParseObject.registerSubclass(EventInvitedMembersParse.class);
        ParseObject.registerSubclass(EventDeclinedMembersParse.class);
        ParseObject.registerSubclass(EventConfirmedMembersParse.class);
        ParseObject.registerSubclass(EventMaybeMembersParse.class);


        ParseObject.registerSubclass(CategoryParse.class);
        ParseObject.registerSubclass(CategoryMembersParse.class);
       // ParseObject.registerSubclass(CategoryMentionsParse.class);

        ParseObject.registerSubclass(PersonParse.class);
       // ParseObject.registerSubclass(PostParse.class);




        //enable local storage
        //Parse.enableLocalDatastore(this);

        //enable email verification



        Parse.initialize(this, parse_app_id, parse_client_key);

      //  PushService.setDefaultPushCallback(this, MainActivity.class);  deprecated
        ParseInstallation.getCurrentInstallation().saveInBackground();  // used to register app for push

        ParseUser.enableAutomaticUser();
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this
        // line.
        defaultACL.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultACL, true);


        FacebookSdk.sdkInitialize(this.getApplicationContext());
        ParseFacebookUtils.initialize(this.getApplicationContext());

    }


    public static void updateParseInstallation(ParseUser user) {
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(KEY_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }



    public static SportsSocialNetworkApplication getsInstance(){
    return sInstance;
    }
    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }
    public static Context getContext()
    {
        return mContext;
    }
/*

    public synchronized  static DBAllEventList getWritableDatabaseAllEventList(){
        if (mDatabaseAllEventList==null){
            mDatabaseAllEventList=new DBAllEventList(getAppContext());
        }

        return mDatabaseAllEventList;
    }
    */

    public static void saveToPreferences(Context context, String preferenceName, String preferenceValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(preferenceName, preferenceValue);
        editor.apply();
    }

    public static void saveToPreferences(Context context, String preferenceName, int preferenceValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(preferenceName, preferenceValue);
        editor.apply();
    }

    public static void saveToPreferences(Context context, String preferenceName, boolean preferenceValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(preferenceName, preferenceValue);
        editor.apply();
    }

    public static String readFromPreferences(Context context, String preferenceName, String defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getString(preferenceName, defaultValue);
    }

    public static boolean readFromPreferences(Context context, String preferenceName, boolean defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getBoolean(preferenceName, defaultValue);
    }
    public static int readFromPreferences(Context context, String preferenceName, int defaultValue) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context.getApplicationContext());
        return sharedPreferences.getInt(preferenceName, defaultValue);
    }

}