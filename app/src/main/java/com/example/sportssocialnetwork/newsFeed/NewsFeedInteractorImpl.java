package com.example.sportssocialnetwork.newsFeed;

import android.util.Log;
import android.widget.Toast;

import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lenovo T420 on 21.11.2015..
 */
public class NewsFeedInteractorImpl implements INewsFeedInteractor,FindCallback<ParseObject>{

    private IonDownloadNewsFeedListFinishedListener mListener;
    private ArrayList<EventParse> listOfEvents;

    public NewsFeedInteractorImpl(IonDownloadNewsFeedListFinishedListener mListener) {
        this.mListener = mListener;

    }
    @Override
    public void obtainNewsFeedList(ArrayList<EventParse> listOfEvents) {
        this.listOfEvents=listOfEvents;

        ParseQuery<ParseObject> query = ParseQuery.getQuery(NewsFeedActivity.TABLE_NAME);
        query.findInBackground(this);
    }
    @Override
    public void done(List<ParseObject> list, ParseException e) {
        if (e == null) {
            listOfEvents.clear();
            for (ParseObject object : list) {
                //  Log.d(SportsSocialNetworkApplication.TAG,object.getString(EVENT_NAME_COLUMN));
                EventParse newEvent = new EventParse();
                newEvent.setEventTitle(object.getString(NewsFeedActivity.EVENT_NAME_COLUMN));
                newEvent.setEventImage(object.getParseFile(NewsFeedActivity.EVENT_IMAGE_COLUMN));
                newEvent.setEventHostName(object.getString(NewsFeedActivity.EVENT_HOST_NAME_COLUMN));
                newEvent.setEventStartDate(object.getString(NewsFeedActivity.EVENT_START_DATE_COLUMN));

                listOfEvents.add(newEvent);
            }
            Log.d(SportsSocialNetworkApplication.TAG, "download news sucess");
            mListener.onDownloadNewsFeedListSucess();

        } else {
            Log.d(SportsSocialNetworkApplication.TAG, "download news failure");
            mListener.onDownloadNewsFeedListFailure(e);
            Toast.makeText(SportsSocialNetworkApplication.getContext(),"Following error occured: +"+ e.getMessage(), Toast.LENGTH_LONG).show();
            //  mListener.onParseloginError(e);

        }
    }
}
