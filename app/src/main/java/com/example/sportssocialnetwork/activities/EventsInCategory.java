package com.example.sportssocialnetwork.activities;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.PersistableBundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.sportssocialnetwork.R;
import com.example.sportssocialnetwork.adapters.AllEventListAdapter;
import com.example.sportssocialnetwork.application.SportsSocialNetworkApplication;
import com.example.sportssocialnetwork.asyncTasks.RetrieveEventsListInCategoryAsyncTask;
import com.example.sportssocialnetwork.callbacks.OnRetreiveEvents;
import com.example.sportssocialnetwork.events.OnRetrieveEventFromParse;
import com.example.sportssocialnetwork.common.BaseActivity;
import com.example.sportssocialnetwork.fragments.DisclaimerDialogFragment;
import com.example.sportssocialnetwork.main.MainActivity;
import com.example.sportssocialnetwork.models.EventParse;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class EventsInCategory extends BaseActivity implements OnRetreiveEvents,OnRetrieveEventFromParse {

    private AllEventListAdapter newsFeedListAdapter;
    private LayoutManager mLayoutManager;
    private ArrayList<EventParse> listOfEvents=new ArrayList<EventParse>();

    private static String TABLE_NAME = "Event";
    private static String EVENT_NAME_COLUMN = "event_title";
    private static String EVENT_IMAGE_COLUMN = "event_image";
    private static String EVENT_HOST_NAME_COLUMN = "event_hostName";
    private static String EVENT_START_DATE_COLUMN = "event_startDate";

    private List<ParseObject> listOfEventsParse;

    @Bind(R.id.app_bar) Toolbar toolbar;
    @Bind(R.id.srNewsFeedList) SwipeRefreshLayout srNewsFeedList;
    @Bind(R.id.rvNewsFeedList)RecyclerView rvNewsFeedList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(SportsSocialNetworkApplication.TAG, "oncreate child class");
        setContentView(R.layout.activity_news_feed);

        setUpView();

    }

    public void setUpView() {
        ButterKnife.bind(this);
          setSupportActionBar(toolbar);
        String categoryName=getIntent().getStringExtra("Category");
        setTitle(categoryName+ " Events");
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvNewsFeedList.setHasFixedSize(true);
        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        rvNewsFeedList.setLayoutManager(mLayoutManager);
        //listOfEvents = ((EventListActivity)getActivity()).onRetrieveAllEventsList();
        listOfEvents =this.onRetrieveAllEventsList();
        new RetrieveEventsListInCategoryAsyncTask(listOfEvents, this,categoryName).execute();

        // specify an adapter (see also next example)
        newsFeedListAdapter = new AllEventListAdapter(listOfEvents, this);
        rvNewsFeedList.setAdapter(newsFeedListAdapter);

    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();

        srNewsFeedList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        RetrieveListAndUpdateView();
                    }
                }).start();
            }
        });


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

    }

    private void RetrieveListAndUpdateView() {


        ParseQuery query = new ParseQuery(TABLE_NAME);
        String categoryName=getIntent().getStringExtra("Category");
        query.whereEqualTo("event_category", categoryName);

        List<EventParse> localList = new ArrayList<EventParse>();
        try {
            List<ParseObject> l = query.find();


            for (ParseObject eventParseObject : l) {

                EventParse newEvent = new EventParse();
                newEvent.setEventTitle(eventParseObject.getString(EVENT_NAME_COLUMN));
                newEvent.setEventImage(eventParseObject.getParseFile(EVENT_IMAGE_COLUMN));
                newEvent.setEventHostName(eventParseObject.getString(EVENT_HOST_NAME_COLUMN));
                newEvent.setEventStartDate(eventParseObject.getString(EVENT_START_DATE_COLUMN));
                localList.add(newEvent);
            }

            listOfEvents.clear();
            listOfEvents.addAll(localList);

        } catch (com.parse.ParseException e) {
        }

        runOnUiThread(
                new Runnable() {
                    @Override
                    public void run() {
                        newsFeedListAdapter.notifyDataSetChanged();
                        srNewsFeedList.setRefreshing(false);
                    }
                }
        );

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_events_in_category, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (id) {

            case R.id.action_back:
                finish();
                break;
            case R.id.action_disclaimer:
                FragmentManager disclaimerManager = getSupportFragmentManager();
                DisclaimerDialogFragment disclaimerDialogFragment = new DisclaimerDialogFragment();
                disclaimerDialogFragment.show(disclaimerManager, "MyDialog");
                break;
            case R.id.action_logout:
                new MaterialDialog.Builder(this)
                        .title(R.string.material_logout_title)
                        .content(R.string.material_logout_content)
                        .positiveText(R.string.material_logout_agree)
                        .negativeText(R.string.material_logout_disagree)
                        .positiveColorRes(R.color.primaryColor)
                        .negativeColorRes(R.color.primaryColor)
                        .titleColorRes(R.color.primaryColor)
                        .dividerColorRes(R.color.divider)
                        .contentColorRes(R.color.secondary_text)
                        .backgroundColorRes(R.color.white)
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                super.onPositive(dialog);
                                ParseUser.logOut();
                                startActivity(new Intent(EventsInCategory.this, MainActivity.class));
                                finish();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                super.onNegative(dialog);
                                dialog.dismiss();
                            }
                        })
                        .show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }




    @Override
    public void OnCompletedTask() {
        newsFeedListAdapter.notifyDataSetChanged();

    }

    @Override
    public ArrayList<EventParse> onRetrieveAllEventsList() {
        return listOfEvents;
    }
}